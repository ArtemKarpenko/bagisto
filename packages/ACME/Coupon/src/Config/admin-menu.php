<?php

return [
    [
    'key'        => 'coupon',
    'name'       => 'Coupon',
    'route'      => 'admin.coupon.index',
    'sort'       => 0,
    'icon-class' => 'temp-icon',
    ], [
        'key'        => 'coupon.coupons',
        'name'       => 'Coupons',
        'route'      => 'admin.coupon.index',
        'sort'       => 1,
        'icon-class' => '',
    ]
];