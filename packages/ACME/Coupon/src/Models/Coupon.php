<?php

namespace ACME\Coupon\Models;

use Illuminate\Database\Eloquent\Model;
use ACME\Coupon\Contracts\Coupon as CouponContract;

class Coupon extends Model implements CouponContract
{
    protected $table = 'coupons';
    protected $fillable = ['id', 'name', 'value', 'status', 'date_expiration'];
}