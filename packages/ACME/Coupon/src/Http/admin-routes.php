<?php

Route::group([
        'prefix'        => 'admin/coupon',
        'middleware'    => ['web', 'admin']
    ], function () {

        Route::get('', 'ACME\Coupon\Http\Controllers\Admin\CouponController@index')->defaults('_config', [
            'view' => 'coupon::admin.coupon.index',
        ])->name('admin.coupon.index');

        Route::get('coupons/create', 'ACME\Coupon\Http\Controllers\Admin\CouponController@create')->defaults('_config', [
            'view' => 'coupon::admin.coupon.create',
        ])->name('admin.coupon.create');

        Route::post('coupons/create', 'ACME\Coupon\Http\Controllers\Admin\CouponController@store')->defaults('_config', [
            'redirect' => 'admin.coupon.index',
        ])->name('admin.coupon.store');

        Route::get('coupons/edit/{id}', 'ACME\Coupon\Http\Controllers\Admin\CouponController@edit')->defaults('_config', [
            'view' => 'coupon::admin.coupon.edit',
        ])->name('admin.coupon.edit');

        Route::post('/coupons/edit/{id}', 'ACME\Coupon\Http\Controllers\Admin\CouponController@update')->defaults('_config', [
            'redirect' => 'admin.coupon.index',
        ])->name('admin.coupon.update');

        Route::post('/coupons/edit/{id}/status', 'ACME\Coupon\Http\Controllers\Admin\CouponController@updateStatus')->defaults('_config', [
            'redirect' => 'admin.coupon.index',
        ])->name('admin.coupon.update-status');

        Route::post('coupons/delete/{id}', 'ACME\Coupon\Http\Controllers\Admin\CouponController@destroy')->name('admin.coupon.delete');

});