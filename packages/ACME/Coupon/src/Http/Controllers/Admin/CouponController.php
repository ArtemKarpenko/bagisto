<?php

namespace ACME\Coupon\Http\Controllers\Admin;

//use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Webkul\Admin\Http\Controllers\Controller;
use ACME\Coupon\Repositories\CouponRepository;

class CouponController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;


    /**
     * ExcursionRepository object
     *
     * @var ACME\Coupon\Repositories\CouponRepository
     */
    protected $couponRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CouponRepository $couponRepository)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->couponRepository = $couponRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name'  => 'required|unique:coupons,name',
            'value' => 'required',
        ]);

        $data = request()->all();

        $this->couponRepository->create($data);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Coupon']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $coupon = $this->couponRepository->findOrFail($id);

        return view($this->_config['view'], compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'name'  => 'required|unique:coupons,name,' . $id,
            'value' => 'required',
        ]);

        $r_all = request()->all();

        $r_all['status'] = !array_key_exists('status', $r_all) ? false : true;

        $this->couponRepository->update($r_all, $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Coupon']));

        return redirect()->route($this->_config['redirect']);
    }

    public function updateStatus($id)
    {
        $coupon = $this->couponRepository->findOrFail($id);

        $val = !$coupon->status;

        $coupon->update(['status' => $val]);

        return response()->json([
            'message' => 'Статус успешно изменен',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->couponRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Coupon']));

            return response()->json(['message' => true], 200);
        } catch(\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Coupon']));
        }

        return response()->json(['message' => false], 400);
    }
}
