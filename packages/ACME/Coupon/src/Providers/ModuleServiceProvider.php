<?php

namespace ACME\Coupon\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \ACME\Coupon\Models\Coupon::class,
    ];
}