<?php

namespace ACME\Coupon\Repositories;

use Webkul\Core\Eloquent\Repository;

class CouponRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'ACME\Coupon\Contracts\Coupon';
    }
}