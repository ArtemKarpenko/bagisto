@extends('admin::layouts.content')

@section('page_title')
    Все купоны
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Купоны</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.coupon.create') }}" class="btn btn-lg btn-primary">
                    Добавить купон
                </a>
            </div>
        </div>

        <div class="page-content">
            <modal id="testModal" :is-open="modalIds.testModal">
                <h3 slot="header">Тестовая модалка</h3>
                <div slot="body">
                    Тело модалки
                </div>
            </modal>

            <div class="btn btn-primary btn-lg" @click="showModal('testModal')">Открыть тестовую модалку</div>
            @inject('couponGroup','ACME\Coupon\Datagrids\CouponDataGrid')
            {!! $couponGroup->render() !!}
        </div>
    </div>

@stop

@push('scripts')

    <script>
        function changeCouponStatus(url, couponId) {
            axios
                .post(url, couponId)
                .then(function (response) {

                });
        }
    </script>
@endpush