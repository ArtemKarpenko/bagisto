@extends('admin::layouts.content')

@section('page_title')
    Добавить купон
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.coupon.update', $coupon->id) }}" @submit.prevent="onSubmit">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="window.location = '{{ route('admin.coupon.index') }}'"></i>

                        Редактировать купон
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Сохранить купон
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                        <label for="name" class="required">
                            Наименование
                        </label>
                        <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') ?: $coupon->name }}" data-vv-as="&quot;Наименование&quot;">
                        <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('value') ? 'has-error' : '']">
                        <label for="name" class="required">
                            Код купона
                        </label>
                        <input type="text" class="control" name="value" v-validate="'required'" value="{{ old('value') ?: $coupon->value }}" data-vv-as="&quot;Код купона&quot;">
                        <span class="control-error" v-if="errors.has('value')">@{{ errors.first('value') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('date_expiration') ? 'has-error' : '']">
                        <label for="dob">Дата истечения</label>
                        <input type="date" class="control" id="dob" name="date_expiration" value="{{ old('date_expiration') ?:$coupon->date_expiration }}" v-validate="" data-vv-as="&quot;Дата истечения&quot;">
                        <span class="control-error" v-if="errors.has('date_expiration')">@{{ errors.first('date_expiration') }}</span>
                    </div>

                    <div class="control-group">
                        <label for="status">Статус</label>

                        <label class="switch">
                            <input type="checkbox" id="status" name="status" value="{{ $coupon->status }}" {{ $coupon->status ? 'checked' : '' }}>
                            <span class="slider round"></span>
                        </label>
                    </div>
                </div>
            </div>

        </form>
    </div>
@stop