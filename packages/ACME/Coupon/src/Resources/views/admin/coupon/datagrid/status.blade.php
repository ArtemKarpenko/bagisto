<span id="coupon-{{ $coupon->id }}-status">
   <div class="control-group">
        <label class="switch" style="margin-top: 0px; margin-bottom: 0px;">
            <input type="checkbox" id="status" name="status" value="{{ $coupon->status }}" {{ $coupon->status ? 'checked' : '' }}>
            <span class="slider round" onclick="changeCouponStatus('{{ route('admin.coupon.update-status', $coupon->id) }}', {{ $coupon->id }})"></span>
        </label>
    </div>
</span>

