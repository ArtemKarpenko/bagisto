<?php

namespace ACME\HelloWorld\Repositories;

use Illuminate\Support\Facades\Storage;
use Webkul\Core\Eloquent\Repository;

class ExcursionRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */

    function model()
    {
        return 'ACME\HelloWorld\Contracts\Excursion';
    }

    /**
     * @param  array  $data
     * @return \ACME\HelloWorld\Contracts\Excursion
     */
    public function create(array $data)
    {
        $excursion = $this->model->create($data);

        $this->uploadImages($data, $excursion);

        return $excursion;
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $id
     * @return \Webkul\Customer\Contracts\CustomerGroup
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $excursion = $this->find($id);

        $excursion->update($data);

        $this->uploadImages($data, $excursion);

        return $excursion;
    }

    public function uploadImages($data, $excursion, $type = "image_path")
    {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'excursion/' . $excursion->id;

                if ($request->hasFile($file)) {
                    if ($excursion->{$type}) {
                        Storage::delete($excursion->{$type});
                    }

                    $excursion->{$type} = $request->file($file)->store($dir);
                    $excursion->save();
                }
            }
        } else {
            if ($excursion->{$type}) {
                Storage::delete($excursion->{$type});
            }

            $excursion->{$type} = null;
            $excursion->save();
        }
    }

    /**
     * Returns guest group.
     *
     * @return object
     */
    public function getCustomerGuestGroup()
    {
        static $customerGuestGroup;

        if ($customerGuestGroup) {
            return $customerGuestGroup;
        }

        return $customerGuestGroup = $this->findOneByField('code', 'guest');
    }
}