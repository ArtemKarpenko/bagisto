<?php

namespace ACME\HelloWorld\Models;

use Illuminate\Database\Eloquent\Model;
use ACME\HelloWorld\Contracts\Excursion as ExcursionContract;

class Excursion extends Model implements ExcursionContract
{
    protected $table = 'excursions';
    protected $fillable = ['id', 'name', 'description', 'url_key', 'sku'];
}