<?php

namespace ACME\HelloWorld\Models;

use Webkul\Product\Models\Product as ProductBaseModel;

class Product extends ProductBaseModel
{
    protected $fillable = [
        'type',
        'attribute_family_id',
        'sku',
        'parent_id',
        'drag_data',
        'color_id',
    ];

    protected $casts = [
        'additional' => 'array',
        'drag_data' => 'array',
    ];
}