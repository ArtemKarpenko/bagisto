// /**
//  * Main imports.
//  */
//
// /**
//  * Vue prototype.
//  */
// Vue.prototype.$http = axios;
//
// /**
//  * Window assignation.
//  */
// window.Vue = Vue;
// window.eventBus = new Vue();

/**
 * Global components.
 */
Vue.component(
    'all-prod-count',
    require('./components/all-prod-count').default
);
Vue.component(
    'excursion',
    require('./components/excursion').default
);
Vue.component('product-collections', require('./UI/components/product-collections').default);
Vue.component('product-card', require('./UI/components/product-card').default);
Vue.component('my-component', require('./UI/components/my-component').default);

// $(function() {
//     Vue.config.ignoredElements = ['option-wrapper', 'group-form', 'group-list'];
//
//     let app = new Vue({
//         el: '#app',
//
//         data: {
//             modalIds: {}
//         },
//
//     });
// });
