@extends('shop::layouts.master')

@section('page_title')
    Все экскурсии
@stop

@section('content-wrapper')

    <div class="main">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="vc-full-screen excursion-slider-wrap">
                        <carousel-component
                            slides-per-page="3"
                            navigation-enabled="hide"
                            pagination-enabled="hide"
                            id="upsell-products-carousel"
                            :slides-count="{{ sizeof($excursions) }}">

                            @foreach ($excursions as $index => $excursion)
                                <slide slot="slide-{{ $index }}">
                                    <div>
                                        <img loading="lazy" src="{{ url()->to('/') . '/storage/' . $excursion->image_path }}" data-src="{{ url()->to('/') . '/storage/' . $excursion->image_path }}" height="500" alt="" class="lzy_img">
                                    </div>
                                    <div>
                                        {{ $excursion->name }}
                                    </div>
                                    <div>
                                        {{ $excursion->description }}
                                    </div>
                                </slide>
                            @endforeach
                        </carousel-component>
                    </div>
                </div>
            </div>
        </div>

    </div>

@stop