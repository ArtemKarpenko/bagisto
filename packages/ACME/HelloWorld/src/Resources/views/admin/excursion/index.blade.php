@extends('admin::layouts.content')

@section('page_title')
    Все экскурсии
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Экскурсии</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.excursion.create') }}" class="btn btn-lg btn-primary">
                    {{ __('helloworld::app.excursions.add-title') }}
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('excursionGroup','ACME\HelloWorld\Datagrids\ExcursionDataGrid')
            {!! $excursionGroup->render() !!}
        </div>
    </div>

@stop