@extends('admin::layouts.content')

@section('page_title')
    Редактировать экскурсию
@stop

@section('content')
    <div class="content">
        <form method="POST" action="" @submit.prevent="onSubmit" enctype="multipart/form-data">
            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="window.location = '{{ route('admin.excursion.index') }}'"></i>

                        Редактировать экскурсию
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Сохранить экскурсию
                    </button>
                </div>
            </div>

            <div class="page-content">

                <div class="form-container">
                    @csrf()

                    <input name="_method" type="hidden" value="PUT">

                    <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                        <label for="name" class="required">
                            Наименование
                        </label>
                        <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') ?: $excursion->name }}" data-vv-as="&quot;Наименование&quot;">
                        <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('sku') ? 'has-error' : '']">
                        <label for="sku">
                            SKU
                        </label>
                        <input type="text" class="control" name="sku" value="{{ old('sku') ?: $excursion->sku }}" data-vv-as="&quot;sku&quot;">
                        <span class="control-error" v-if="errors.has('sku')">@{{ errors.first('sku') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('description') ? 'has-error' : '']">
                        <label for="description">
                            description
                        </label>
                        <input type="text" class="control" name="description" value="{{ old('description') ?: $excursion->description }}" data-vv-as="&quot;sku&quot;">
                        <span class="control-error" v-if="errors.has('description')">@{{ errors.first('description') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('url_key') ? 'has-error' : '']">
                        <label for="url_key">
                            url_key
                        </label>
                        <input type="text" class="control" name="url_key" value="{{ old('url_key') ?: $excursion->url_key }}" data-vv-as="&quot;sku&quot;">
                        <span class="control-error" v-if="errors.has('url_key')">@{{ errors.first('url_key') }}</span>
                    </div>

                    <div class="control-group">
                        <label>Изображение</label>

                        @if (isset($excursion) && $excursion->image_path)
                            <image-wrapper
                                :multiple="false"
                                input-name="image_path"
                                :images='"{{ url()->to('/') . '/storage/' . $excursion->image_path }}"'
                                :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'">
                            </image-wrapper>
                        @else
                            <image-wrapper
                                :multiple="false"
                                input-name="image_path"
                                :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'">
                            </image-wrapper>
                        @endif

                    </div>
                </div>
            </div>
        </form>
    </div>
@stop