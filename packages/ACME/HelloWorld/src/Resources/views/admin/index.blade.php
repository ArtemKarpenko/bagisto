@extends('admin::layouts.master')

@section('page_title')
    Package HelloWorld
@stop

@section('content-wrapper')

    <div class="content full-page dashboard">
        <div class="page-header">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="page-title">
                            <h1>Новый пакет</h1>
                        </div>
                    </div>
                </div>
            </div>


            <div class="container-fluid mt-3">
                <div class="row">
                    <div class="col-12">
                        @include ('helloworld::admin.test')
                        Все экскурсии:<br>
                        <excursion :excursions="{{ json_encode($excursions) }}"></excursion>
                        <a href="{{ route('admin.excursion.index') }}">К таблице</a>
                    </div>
                </div>

            </div>

            <div class="page-action">
            </div>
        </div>

        <div class="page-content">
        </div>
    </div>

@stop

