<?php

return [
    [
        'key' => 'helloworld',
        'name' => 'New package',
        'route' => 'admin.helloworld.index',
        'sort' => 1,
        'icon-class' => 'dashboard-icon',
    ], [
        'key' => 'helloworld.excursions',
        'name' => 'Экскурсии',
        'route' => 'admin.excursion.index',
        'sort' => 0,
        'icon-class' => '',
    ]
];