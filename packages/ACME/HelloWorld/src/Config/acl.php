<?php

return [
    [
        'key' => 'helloworld',
        'name' => 'New package',
        'route' => 'admin.helloworld.index',
        'sort' => 1,
    ], [
        'key' => 'helloworld.excursions',
        'name' => 'Экскурсии',
        'route' => 'admin.excursion.index',
        'sort' => 0,
    ]
];