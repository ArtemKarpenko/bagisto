<?php

namespace ACME\HelloWorld\Providers;

use Webkul\Core\Providers\CoreModuleServiceProvider;

//use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends CoreModuleServiceProvider
{
    protected $models = [
        \ACME\HelloWorld\Models\HelloWorld::class,
        \ACME\HelloWorld\Models\Excursion::class,
    ];
}