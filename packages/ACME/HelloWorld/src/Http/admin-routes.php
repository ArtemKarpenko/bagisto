<?php

Route::group([
        'prefix'        => 'admin/helloworld',
        'middleware'    => ['web', 'admin']
    ], function () {

        Route::get('', 'ACME\HelloWorld\Http\Controllers\Admin\HelloWorldController@index')->defaults('_config', [
            'view' => 'helloworld::admin.index',
        ])->name('admin.helloworld.index');

        Route::get('excursions/edit/{id}', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@edit')->defaults('_config', [
            'view' => 'helloworld::admin.excursion.edit',
        ])->name('admin.excursion.edit');

//        Route::put('groups/edit/{id}', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@update')->defaults('_config', [
//            'redirect' => 'admin.excursion.index',
//        ])->name('admin.excursion.update');

//        Route::post('/excursions/edit/{id}', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@updateput')->defaults('_config', [
//            'redirect' => 'admin.excursion.index',
//        ])->name('admin.excursion.updateput');

        Route::put('/excursions/edit/{id}', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@updateput')->defaults('_config', [
            'redirect' => 'admin.excursion.index',
        ])->name('admin.excursion.updateput');

        Route::get('excursions', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@index')->defaults('_config', [
            'view' => 'helloworld::admin.excursion.index',
        ])->name('admin.excursion.index');

        Route::get('excursions/create', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@create')->defaults('_config', [
            'view' => 'helloworld::admin.excursion.create',
        ])->name('admin.excursion.create');

        Route::post('excursions/create', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@store')->defaults('_config', [
            'redirect' => 'admin.excursion.index',
        ])->name('admin.excursion.store');

        Route::post('excursions/delete/{id}', 'ACME\HelloWorld\Http\Controllers\Admin\ExcursionController@destroy')->name('admin.excursion.delete');

});