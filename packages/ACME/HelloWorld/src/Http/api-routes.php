<?php

Route::group(['prefix' => 'api'], function ($router) {
    Route::group(['middleware' => ['locale', 'theme', 'currency']], function ($router) {
        Route::get('excursions', 'ACME\HelloWorld\Http\Controllers\Api\ExcursionController@all');
    });
});
