<?php

namespace ACME\HelloWorld\Http\Controllers\Api;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Webkul\Customer\Http\Requests\CustomerRegistrationRequest;
use Webkul\Customer\Repositories\CustomerGroupRepository;
use Webkul\Customer\Repositories\CustomerRepository;
use ACME\HelloWorld\Repositories\ExcursionRepository;

class ExcursionController extends Controller
{
    protected $excursionRepository;

    public function __construct(ExcursionRepository $excursionRepository)
    {
        $this->excursionRepository = $excursionRepository;
    }

    public function all(){
        return $this->excursionRepository->all();
    }
}