<?php

namespace ACME\HelloWorld\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use ACME\HelloWorld\Models\HelloWorld;
use Webkul\Product\Models\Product;
use Illuminate\Support\Facades\Log;
use ACME\HelloWorld\Repositories\ExcursionRepository;

class HelloWorldController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ExcursionRepository object
     *
     * @var ACME\HelloWorld\Repositories\ExcursionRepository
     */
    protected $excursionRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ExcursionRepository $excursionRepository)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->excursionRepository = $excursionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(ExcursionRepository $excursionRepository)
    {
        $excursions = $this->excursionRepository->get();
        $excursions = json_encode($excursions);
        return view($this->_config['view'], compact('excursions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        return view($this->_config['view']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
