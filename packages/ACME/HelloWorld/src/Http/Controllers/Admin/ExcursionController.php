<?php

namespace ACME\HelloWorld\Http\Controllers\Admin;

use Illuminate\Support\Facades\Redirect;
use Webkul\Admin\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use ACME\HelloWorld\Models\HelloWorld;
use Webkul\Product\Models\Product;
use Illuminate\Support\Facades\Log;
use ACME\HelloWorld\Repositories\ExcursionRepository;

class ExcursionController extends Controller
{

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ExcursionRepository object
     *
     * @var ACME\HelloWorld\Repositories\ExcursionRepository
     */
    protected $excursionRepository;

    /**
     * Create a new controller instance.
     *
     * @param  ACME\HelloWorld\Repositories\ExcursionRepository  $excursionRepository;
     * @return void
     */
    public function __construct(ExcursionRepository $excursionRepository)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->excursionRepository = $excursionRepository;
    }

    public function all() {
        return '123';
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
//        $this->validate(request(), [
//            'name' => 'required',
//        ]);

        $data = request()->all();

        $data['is_user_defined'] = 1;

        $this->excursionRepository->create($data);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Excursion']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $excursion = $this->excursionRepository->findOrFail($id);

        return view($this->_config['view'], compact('excursion'));
    }


    public function updateput($id)
    {
        $this->excursionRepository->update(request()->all(), $id);

        session()->flash('success', trans('Экскурсия успешно обновлена', ['name' => 'Excursion']));

//        return Redirect::back();

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'name' => 'required',
        ]);

        $this->excursionRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Excursion']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $excursion = $this->excursionRepository->findOrFail($id);

        try {
            $this->excursionRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Excursion']));

            return response()->json(['message' => true], 200);
        } catch(\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Excursion']));
        }

        return response()->json(['message' => false], 400);
    }
}
