<?php

namespace ACME\HelloWorld\Http\Controllers\Shop;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use ACME\HelloWorld\Repositories\ExcursionRepository;
use ACME\HelloWorld\Models\Excursion;

class HelloWorldController extends Controller
{
    use DispatchesJobs, ValidatesRequests;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $excursions = Excursion::all();
        return view($this->_config['view'], compact('excursions'));
    }
}
