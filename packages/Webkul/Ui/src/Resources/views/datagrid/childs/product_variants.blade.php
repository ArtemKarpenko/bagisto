@if(isset($record->variants))
    @foreach($record->variants as $variant)
        <tr class="box">
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $variant->size }} размер</td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $variant->quantity }} шт.</td>
            <td>{{ $variant->price }} ₽</td>
            <td></td>
            <td></td>
        </tr>
    @endforeach
@endif