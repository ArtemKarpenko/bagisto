<?php

return [
    [
        'key' => 'lookbook',
        'name' => 'Лукбуки',
        'route' => 'admin.lookbook.index',
        'sort' => 2,
        'icon-class' => 'temp-icon',
    ], [
        'key'        => 'lookbook.lookbooks',
        'name'       => 'Лукбуки',
        'route'      => 'admin.lookbook.index',
        'sort'       => 1,
        'icon-class' => '',
    ]
];