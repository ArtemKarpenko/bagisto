<?php

return [
    [
        'key' => 'lookbook',
        'name' => 'LookBook',
        'route' => 'admin.lookbook.index',
        'sort' => 2
    ]
];