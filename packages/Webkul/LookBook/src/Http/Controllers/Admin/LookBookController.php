<?php

namespace Webkul\LookBook\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Storage;
use Webkul\LookBook\Repositories\LookBookRepository;
use Webkul\LookBook\Repositories\LookBookPageRepository;
use Webkul\LookBook\DataGrids\LookBookPagesDataGrid;
use Webkul\LookBook\Models\LookBookPage;

class LookBookController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * LookBookRepository object
     *
     * @var Webkul\LookBook\Repositories\LookBookRepository
     */
    protected $lookBookRepository;

    /**
     * LookBookRepository object
     *
     * @var Webkul\LookBook\Repositories\LookBookPageRepository
     */
    protected $lookBookPageRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(LookBookRepository $lookBookRepository, LookBookPageRepository $lookBookPageRepository)
    {
        $this->middleware('admin');

        $this->lookBookRepository = $lookBookRepository;

        $this->lookBookPageRepository = $lookBookPageRepository;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'name'  => 'required',
        ]);

        $data = request()->all();

        $lookBook = $this->lookBookRepository->create($data);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Лукбук']));

        return redirect()->route($this->_config['redirect'], ['id' => $lookBook->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $lookbook = $this->lookBookRepository->findOrFail($id);

        return view($this->_config['view'], compact('lookbook'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $this->validate(request(), [
            'name'  => 'required',
        ]);

        $data = request()->all();

        $data['status'] = !array_key_exists('status', $data) ? false : true;

        $this->lookBookRepository->update($data, $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Лукбук']));

        return redirect()->route($this->_config['redirect']);
    }

    public function updateStatus($id)
    {
        $lookbook = $this->lookBookRepository->findOrFail($id);

        $val = !$lookbook->status;

        $lookbook->update(['status' => $val]);

        return response()->json([
            'message' => 'Статус успешно изменен',
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $this->lookBookRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Лукбук']));

            return response()->json(['message' => true], 200);
        } catch(\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Лукбук']));
        }

        return response()->json(['message' => false], 400);
    }

    public function itemsDataGrid($id, $type = null)
    {
        if (request()->ajax()) {
            return app(LookBookPagesDataGrid::class)->toJson();
        }

        return view($this->_config['view']);
    }

    /**
     * Создание новой страницы лукбука
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewPage($id)
    {
        $pages = $this->lookBookRepository->find($id)->pages();
        if (!empty($pages)){
            $nextSort = $pages->max('sort') + 1;
        }else{
            $nextSort = 1;
        }

        $lookBookPage = $this->lookBookPageRepository->create([
            'name' => 'page 1',
            'look_book_id' => $id,
            'sort' => $nextSort,
        ]);
        return response()->json(['message' => 'Страница успешно создана', 'pageId' => $lookBookPage->id, 'sort' => $nextSort], 200);
    }

    /**
     * Сортировка страниц лукбука при перетаскивании.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pageSort()
    {
        $items = request()->all();
        foreach ($items as $item) {
            $this->lookBookPageRepository->find($item['page_id'])->update(['sort' => $item['index']]);
        }
        return response()->json(['message' => 'Сортировка страниц лукбука успешно изменена'], 200);
    }

    /**
     * Добавление продуктов к странице лукбука.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pageAddItems($pageId)
    {
        $lookBookPage = $this->lookBookPageRepository->findOrFail($pageId);
        $data = request()->all();
        $productIds = explode(',', $data['indexes']);
        foreach ($productIds as $productId) {
            $lookBookPage->products()->create(['child_id' => $productId, 'parent_id' => $pageId]);
        }
        return response()->json(['message' => 'Продукты успешно связаны', 'pageId' => $pageId], 200);
    }

    /**
     * Удаление продуктов из странице лукбука.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pageDeleteItems($pageId)
    {
        $lookBookPage = $this->lookBookPageRepository->findOrFail($pageId);
        $data = request()->all();
        $productIds = explode(',', $data['indexes']);
        $lookBookPage->products()->whereIn('child_id', $productIds)->where('parent_id', $pageId)->delete();
        return response()->json(['message' => 'Связь с продуктами успешно удалена', 'pageId' => $pageId], 200);
    }

    /**
     * Сортировка продуктов у страницы лукбука при перетаскивании.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pageDragItems($pageId) {
        $lookBookPage = $this->lookBookPageRepository->findOrFail($pageId);
        try {
            $items = request()->all();
            foreach ($items as $key => $item) {
                $lookBookPage->products()->where('child_id', $item['id'])->update([
                    'sort' => $item['index'],
                ]);
            }
            return response()->json(['message' => 'Сортировка продуктов успешно изменена'], 200);
        } catch (Exception $e) {
            report($e);
            return response()->json(['message' => 'При сортировке продуктов возникла ошибка'], 400);
        }
    }

    /**
     * Удаление страницы лукбука.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function pageDelete()
    {
        try {
            $data = request()->all();
            $this->lookBookPageRepository->findOrFail($data['pageId'])->delete();
            return response()->json(['message' => 'Страница лукбука успешно удалена'], 200);
        } catch (Exception $e) {
            report($e);
            return response()->json(['message' => 'При удалении страницы лукбука возникла ошибка'], 400);
        }
    }

    /**
     * Залитие картинки страницы лукбука на сервер, запись пути к картинке в БД, удаление старой картинки
     *
     * @return void
     */
    public function uploadImage($id)
    {
        $request = request();
        $dir = 'lookBookPage/' . $id;
        $lookBookPage = $this->lookBookPageRepository->find($id);
        if ($lookBookPage->image) {
            Storage::delete($lookBookPage->image);
        }
        if ($request->hasFile('file')) {
            $path = $request->file('file')->store($dir);
            $lookBookPage->image = $path;
            $lookBookPage->save();
        }
    }
}
