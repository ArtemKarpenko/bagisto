<?php

Route::group([
        'prefix'        => 'admin/lookbook',
        'middleware'    => ['web', 'admin']
    ], function () {

        Route::get('', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@index')->defaults('_config', [
            'view' => 'lookbook::admin.lookbook.index',
        ])->name('admin.lookbook.index');

        Route::get('/create', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@create')->defaults('_config', [
            'view' => 'lookbook::admin.lookbook.create',
        ])->name('admin.lookbook.create');

        Route::post('/create', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@store')->defaults('_config', [
            'redirect' => 'admin.lookbook.edit',
        ])->name('admin.lookbook.store');

        Route::post('/add-new-page/{id}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@addNewPage')->defaults('_config', [
        ])->name('admin.lookbook.add-new-page');

        Route::get('/edit/{id}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@edit')->defaults('_config', [
            'view' => 'lookbook::admin.lookbook.edit',
        ])->name('admin.lookbook.edit');

        Route::post('/edit/{id}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@update')->defaults('_config', [
            'redirect' => 'admin.lookbook.index',
        ])->name('admin.lookbook.update');

        Route::post('/delete/{id}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@destroy')->name('admin.lookbook.delete');

        Route::post('/edit/{id}/status', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@updateStatus')->defaults('_config', [
        ])->name('admin.lookbook.update-status');

        //datagrid for look books pages items
        Route::get('/items-data-grid/{pageId}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@itemsDataGrid')->defaults('_config', [
            'view' => 'lookbook::admin.lookbook.datagrids.items',
        ])->name('admin.catalog.products.items-data-grid');

        Route::get('/items-data-grid/{pageId}/{type}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@itemsDataGrid')->defaults('_config', [
            'view' => 'lookbook::admin.lookbook.datagrids.items',
        ])->name('admin.catalog.products.items-data-grid-type');

        Route::prefix('pages')->group(function () {
            Route::post('/upload-image/{id}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@uploadImage')->name('admin.lookbook.pages.upload-image');
            Route::post('/delete', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@pageDelete')->name('admin.lookbook.pages.delete');
            Route::post('/sort', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@pageSort')->name('admin.lookbook.pages.sort');
            Route::post('/add-items/{pageId}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@pageAddItems')->name('admin.lookbook.pages.add-items');
            Route::post('/delete-items/{pageId}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@pageDeleteItems')->name('admin.lookbook.pages.delete-items');
            Route::post('/drag-items/{pageId}', 'Webkul\LookBook\Http\Controllers\Admin\LookBookController@pageDragItems')->name('admin.lookbook.pages.drag-items');
        });

});