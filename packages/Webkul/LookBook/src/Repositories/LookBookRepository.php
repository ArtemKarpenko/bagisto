<?php

namespace Webkul\LookBook\Repositories;

use Illuminate\Support\Facades\Storage;
use Webkul\Core\Eloquent\Repository;

class LookBookRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\LookBook\Contracts\LookBook';
    }

    /**
     * @param  array  $data
     * @param  int  $id
     * @param  string  $attribute
     * @return \Webkul\LookBook\Contracts\LookBook
     */
    public function update(array $data, $id, $attribute = "id")
    {
        $lookbook = $this->find($id);

        $lookbook = parent::update($data, $id, $attribute);

        $this->uploadImages($data, $lookbook);

        $this->uploadImages($data, $lookbook, 'image_main_1');

        $this->uploadImages($data, $lookbook, 'image_main_2');

        return $lookbook;
    }

    /**
     * @param  array  $data
     * @return \Webkul\LookBook\Contracts\LookBook
     */
    public function create(array $data)
    {
        $lookbook = parent::create($data);

        $this->uploadImages($data, $lookbook);

        $this->uploadImages($data, $lookbook, 'image_main_1');

        $this->uploadImages($data, $lookbook, 'image_main_2');

        return $lookbook;
    }

    /**
     * @param  array  $data
     * @param  \Webkul\LookBook\Contracts\LookBook  $lookbook
     * @param  string  $type
     * @return void
     */
    public function uploadImages($data, $lookbook, $type = "image_general")
    {
        if (isset($data[$type])) {
            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'look-book/' . $lookbook->id;

                if (request()->hasFile($file)) {
                    if ($lookbook->{$type}) {
                        Storage::delete($lookbook->{$type});
                    }

                    $lookbook->{$type} = request()->file($file)->store($dir);
                    $lookbook->save();
                }
            }
        } else {
            if ($lookbook->{$type}) {
                Storage::delete($lookbook->{$type});
            }

            $lookbook->{$type} = null;
            $lookbook->save();
        }
    }


}