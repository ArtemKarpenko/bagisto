<?php

namespace Webkul\LookBook\Repositories;

use Webkul\Core\Eloquent\Repository;

class LookBookPageRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\LookBook\Contracts\LookBookPage';
    }
}