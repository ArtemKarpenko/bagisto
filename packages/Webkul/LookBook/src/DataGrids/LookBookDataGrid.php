<?php

namespace Webkul\LookBook\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;
use Webkul\LookBook\Repositories\LookBookRepository;

class LookBookDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    /**
     * Look book repository instance.
     *
     * @var \Webkul\LookBook\Repositories\LookBookRepository
     */
    protected $lookBookRepository;

    public function __construct(
        LookBookRepository $lookBookRepository
    ) {
        parent::__construct();

        $this->lookBookRepository = $lookBookRepository;
    }

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('look_books')->addSelect('id', 'name', 'description', 'status', 'image_general');

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => 'Id',
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => '',
            'label'      => 'Фото',
            'type'       => 'string',
            'sortable'   => false,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                return $this->renderImageView($row);
            },
        ]);

        $this->addColumn([
            'index'      => 'name',
            'label'      => 'Наименование',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'status',
            'label'      => 'status',
            'type'       => 'boolean',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => false,
            'closure'    => function ($row) {
                return $this->renderStatusView($row);
            },
        ]);

        $this->addColumn([
            'index'      => 'description',
            'label'      => 'Описание',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'title'  => trans('admin::app.datagrid.edit'),
            'method' => 'GET',
            'route'  => 'admin.lookbook.edit',
            'icon'   => 'icon pencil-lg-icon',
        ]);

        $this->addAction([
            'title'  => trans('admin::app.datagrid.delete'),
            'method' => 'POST',
            'route'  => 'admin.lookbook.delete',
            'icon'   => 'icon trash-icon',
        ]);
    }

    private function renderImageView($row)
    {
        $lookbook = $this->lookBookRepository->find($row->id);

        $image = $lookbook->image_general_url();

        $id = $row->id;

        return view('lookbook::admin.lookbook.datagrid.image', compact('image', 'id'))->render();
    }

    private function renderStatusView($row)
    {
        $lookbook = $row;
        return view('lookbook::admin.lookbook.datagrid.status', compact('lookbook'))->render();
    }
}