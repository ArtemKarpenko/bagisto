<?php

namespace Webkul\LookBook\Observers;

use Illuminate\Support\Facades\Storage;
use Webkul\LookBook\Models\LookBook;
use Carbon\Carbon;

class LookBookObserver
{
    /**
     * Handle the Look book "deleted" event.
     *
     * @param  \Webkul\LookBook\Contracts\LookBook  $lookbook
     * @return void
     */
    public function deleted($lookbook)
    {
        Storage::deleteDirectory('look-book/' . $lookbook->id);
    }
}