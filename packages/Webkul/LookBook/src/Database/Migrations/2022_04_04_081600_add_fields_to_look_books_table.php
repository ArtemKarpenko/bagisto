<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToLookBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('look_books', function (Blueprint $table) {
            $table->boolean('status')->default(0)->comment('Статус видимости лукбука');
            $table->string('image_general')->nullable()->comment('Основная обложка');
            $table->string('image_main_1')->nullable()->comment('Обл. для главной-1');
            $table->string('image_main_2')->nullable()->comment('Обл. для главной-2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('look_books', function (Blueprint $table) {
            $table->dropColumn('status', 'image', 'image_main_1', 'image_main_2');
        });
    }
}
