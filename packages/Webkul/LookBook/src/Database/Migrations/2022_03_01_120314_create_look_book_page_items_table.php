<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLookBookPageItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('look_book_page_items', function (Blueprint $table) {
            $table->integer('parent_id')->unsigned()->comment('Идентификатор страницы лукбука');
            $table->integer('child_id')->unsigned()->comment('Идентификатор продукта');
            $table->foreign('parent_id')->references('id')->on('look_book_pages')->onDelete('cascade');
            $table->foreign('child_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('look_book_page_items');
    }
}
