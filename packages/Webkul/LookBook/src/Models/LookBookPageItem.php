<?php

namespace Webkul\LookBook\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\LookBook\Contracts\LookBookPageItem as LookBookPageItemContract;

class LookBookPageItem extends Model implements LookBookPageItemContract
{
    public $timestamps = false;
    protected $fillable = ['child_id', 'parent_id', 'sort'];
}