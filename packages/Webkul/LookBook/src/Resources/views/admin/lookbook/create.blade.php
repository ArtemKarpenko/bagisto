@extends('admin::layouts.content')

@section('page_title')
    Добавить лукбук
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.lookbook.store') }}" @submit.prevent="onSubmit" enctype="multipart/form-data">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="window.location = '{{ route('admin.lookbook.index') }}'"></i>

                        Добавить лукбук
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Сохранить лукбук
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <div class="control-group" :class="[errors.has('name') ? 'has-error' : '']">
                        <label for="name" class="required">
                            Наименование
                        </label>
                        <input type="text" class="control" name="name" v-validate="'required'" value="{{ old('name') }}" data-vv-as="&quot;Наименование&quot;">
                        <span class="control-error" v-if="errors.has('name')">@{{ errors.first('name') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('description') ? 'has-error' : '']">
                        <label for="description">
                            Описание
                        </label>
                        <textarea name="description" data-vv-as="&quot;Описание&quot;" class="control">{{ old('description') }}</textarea>
                        <span class="control-error" v-if="errors.has('description')">@{{ errors.first('description') }}</span>
                    </div>

                    <div class="control-group">
                        <label for="status">Статус</label>

                        <label class="switch">
                            <input type="checkbox" id="status" name="status" value="1">
                            <span class="slider round"></span>
                        </label>
                    </div>

                    <div class="control-group">
                        <label>Основная обложка</label>
                        <image-wrapper :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'" input-name="image_general" :multiple="false"></image-wrapper>
                    </div>

                    <div class="control-group">
                        <label>Обл. для главной-1</label>
                        <image-wrapper :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'" input-name="image_main_1" :multiple="false"></image-wrapper>
                    </div>

                    <div class="control-group">
                        <label>Обл. для главной-2</label>
                        <image-wrapper :button-label="'{{ __('admin::app.catalog.products.add-image-btn-title') }}'" input-name="image_main_2" :multiple="false"></image-wrapper>
                    </div>

                </div>
            </div>

        </form>
    </div>
@stop