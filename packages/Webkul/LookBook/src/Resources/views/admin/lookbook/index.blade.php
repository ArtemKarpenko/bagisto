@extends('admin::layouts.content')

@section('page_title')
    Все лукбуки
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Лукбуки</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.lookbook.create') }}" class="btn btn-lg btn-primary">
                    Добавить лукбук
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('lookBookGroup','Webkul\LookBook\DataGrids\LookBookDataGrid')
            {!! $lookBookGroup->render() !!}
        </div>
    </div>

@stop

@push('scripts')

    <script>
        function changeLookBookStatus(url, lookbookId) {
            axios
                .post(url, lookbookId)
                .then(function (response) {

                });
        }
    </script>
@endpush