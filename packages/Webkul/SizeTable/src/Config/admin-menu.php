<?php

return [
    [
    'key' => 'sizetable',
    'name' => 'Размерные таблицы',
    'route' => 'admin.sizetable.index',
    'sort' => 2,
    'icon-class' => 'temp-icon',
    ], [
        'key'        => 'sizetable.sizetables',
        'name'       => 'Размерные таблицы',
        'route'      => 'admin.sizetable.index',
        'sort'       => 1,
        'icon-class' => '',
    ]
];