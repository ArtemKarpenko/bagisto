<?php

namespace Webkul\SizeTable\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Webkul\SizeTable\Models\SizeTable::class,
    ];
}