<?php

Route::group([
        'prefix'        => 'admin/sizetable',
        'middleware'    => ['web', 'admin']
    ], function () {

        Route::get('', 'Webkul\SizeTable\Http\Controllers\Admin\SizeTableController@index')->defaults('_config', [
            'view' => 'sizetable::admin.sizetable.index',
        ])->name('admin.sizetable.index');

        Route::get('/create', 'Webkul\SizeTable\Http\Controllers\Admin\SizeTableController@create')->defaults('_config', [
            'view' => 'sizetable::admin.sizetable.create',
        ])->name('admin.sizetable.create');

        Route::post('/create', 'Webkul\SizeTable\Http\Controllers\Admin\SizeTableController@store')->defaults('_config', [
            'redirect' => 'admin.sizetable.index',
        ])->name('admin.sizetable.store');

        Route::get('/edit/{id}', 'Webkul\SizeTable\Http\Controllers\Admin\SizeTableController@edit')->defaults('_config', [
            'view' => 'sizetable::admin.sizetable.edit',
        ])->name('admin.sizetable.edit');

        Route::post('/edit/{id}', 'Webkul\SizeTable\Http\Controllers\Admin\SizeTableController@update')->defaults('_config', [
            'redirect' => 'admin.sizetable.index',
        ])->name('admin.sizetable.update');

        Route::post('/delete/{id}', 'Webkul\SizeTable\Http\Controllers\Admin\SizeTableController@destroy')->name('admin.sizetable.delete');

});