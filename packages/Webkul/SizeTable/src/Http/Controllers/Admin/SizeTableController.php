<?php

namespace Webkul\SizeTable\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Webkul\SizeTable\Repositories\SizeTableRepository;

class SizeTableController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ExcursionRepository object
     *
     * @var Webkul\SizeTable\Repositories\SizeTableRepository
     */
    protected $sizeTableRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SizeTableRepository $sizeTableRepository)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->sizeTableRepository = $sizeTableRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'title' => 'required',
        ]);

        $data = request()->all();

        $this->sizeTableRepository->create($data);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Размерная таблица']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $sizetable = $this->sizeTableRepository->findOrFail($id);

        return view($this->_config['view'], compact('sizetable'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'title' => 'required',
        ]);

        $this->sizeTableRepository->update(request()->all(), $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Размерная таблица']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
