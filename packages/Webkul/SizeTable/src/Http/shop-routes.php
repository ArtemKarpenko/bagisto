<?php

Route::group([
        'prefix'     => 'sizetable',
        'middleware' => ['web', 'theme', 'locale', 'currency']
    ], function () {

        Route::get('/', 'Webkul\SizeTable\Http\Controllers\Shop\SizeTableController@index')->defaults('_config', [
            'view' => 'sizetable::shop.index',
        ])->name('shop.sizetable.index');

});