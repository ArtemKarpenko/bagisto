<?php

namespace Webkul\Product\Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class ManualsTableSeeder extends Seeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('care_manuals')->delete();

        DB::table('care_manuals')->insert([
            [
                'id'              => '1',
                '1c_id'            => '61edd415-742c-4065-8edf-5c139a29755f',
                'value'            => 'Уход 1',
            ],
            [
                'id'              => '2',
                '1c_id'            => '61edd415-742c-4060-8edf-5c139a29755f',
                'value'            => 'Уход 2',
            ],
            [
                'id'              => '3',
                '1c_id'            => '61edd413-742c-4065-8edf-5c139a29755f',
                'value'            => 'Уход 3',
            ]
        ]);

        DB::table('components_manuals')->delete();

        DB::table('components_manuals')->insert([
            [
                'id'              => '1',
                '1c_id'            => '61ere415-742c-4065-8edf-5c139a29755f',
                'value'            => 'Состав товара 1',
            ],
            [
                'id'              => '2',
                '1c_id'            => '61edd415-7t2c-4060-8edf-5c139a29755f',
                'value'            => 'Состав товара 2',
            ],
            [
                'id'              => '3',
                '1c_id'            => '61edd413-742c-4gh5-8edf-5c139a29755f',
                'value'            => 'Состав товара 3',
            ]
        ]);

        DB::table('textile_manuals')->delete();

        DB::table('textile_manuals')->insert([
            [
                'id'              => '1',
                '1c_id'            => '61ere419-742c-4065-8edf-5c139a20755f',
                'value'            => 'Ткань 1',
            ],
            [
                'id'              => '2',
                '1c_id'            => '61ed6415-7t2c-4360-8edf-5c132a29755f',
                'value'            => 'Ткань 2',
            ],
            [
                'id'              => '3',
                '1c_id'            => '11edd413-742c-4gh5-8edf-5c139a29755a',
                'value'            => 'Ткань 3',
            ]
        ]);

        DB::table('view_types')->delete();

        DB::table('view_types')->insert([
            [
                'id'              => '1',
                '1c_id'            => '61edf419-742c-4065-1edf-5c139a20755f',
                'value'            => 'Верхняя одежда',
                'parent_id'        => null,
            ],
            [
                'id'              => '2',
                '1c_id'            => '61ed6412-7t2c-4360-8edf-5c132a29755f',
                'value'            => 'Зимняя',
                'parent_id'        => '1',
            ],
            [
                'id'              => '3',
                '1c_id'            => '61zx6412-7t2c-4360-8edf-5c132a29755f',
                'value'            => 'Летняя',
                'parent_id'        => '1',
            ],
            [
                'id'              => '4',
                '1c_id'            => '61zx6412-1t2c-4234-8edf-5c132a29755f',
                'value'            => 'Демисезонная',
                'parent_id'        => '1',
            ],
            [
                'id'              => '5',
                '1c_id'            => '00zx6412-1t2c-4234-8edf-5c132a29755d',
                'value'            => 'Комбинезоны',
                'parent_id'        => null,
            ],
            [
                'id'              => '6',
                '1c_id'            => '11zx6412-1t2c-2234-8edf-5c132a29755e',
                'value'            => 'Песочники',
                'parent_id'        => null,
            ],
        ]);

        DB::table('colors')->delete();

        DB::table('colors')->insert([
            [
                'id'              => '1',
                '1c_id'            => '61edf419-755c-4065-1edf-5c139a20755f',
                'title'            => 'Красный',
                'parent_id'        => null,
                'hex_code'         => '#ff0000'
            ],
            [
                'id'              => '2',
                '1c_id'            => '61edf410-755c-4065-1ede-5z139a20755f',
                'title'            => 'Синий',
                'parent_id'        => null,
                'hex_code'         => '#0000ff'
            ],
            [
                'id'              => '3',
                '1c_id'            => '61edf410-755c-4005-1edf-5c139a20750f',
                'title'            => 'Зелёный',
                'parent_id'        => null,
                'hex_code'         => '#00ff00'
            ],
        ]);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}