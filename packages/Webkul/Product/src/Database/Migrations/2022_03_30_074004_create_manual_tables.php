<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateManualTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('care_manuals', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор справочника по уходу за товарами');
            $table->string('1c_id')->comment('uuid из 1С');
            $table->text('value')->comment('Значение записи в справочнике');
        });

        Schema::create('textile_manuals', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор справочника по тканям');
            $table->string('1c_id')->comment('uuid из 1С');
            $table->text('value')->comment('Значение записи в справочнике');
        });

        Schema::create('components_manuals', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор справочника по составам товаров');
            $table->string('1c_id')->comment('uuid из 1С');
            $table->text('value')->comment('Значение записи в справочнике');
        });

        Schema::create('view_types', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор справочника вида / типа одежды');
            $table->string('1c_id')->comment('uuid из 1С');
            $table->text('value')->comment('Значение записи в справочнике');
            $table->integer('parent_id')->unsigned()->nullable();
        });

        Schema::table('view_types', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('view_types');
        });

        Schema::table('products', function (Blueprint $table) {
            $table->integer('view_type_id')->unsigned()->nullable();
            $table->foreign('view_type_id')->references('id')->on('view_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('care_manuals');
        Schema::dropIfExists('textile_manuals');
        Schema::dropIfExists('components_manuals');
        Schema::dropIfExists('view_types');
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('view_type_id');
        });
    }
}
