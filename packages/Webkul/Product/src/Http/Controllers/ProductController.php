<?php

namespace Webkul\Product\Http\Controllers;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Webkul\Admin\DataGrids\ProductAjaxDataGrid;
use Webkul\Attribute\Repositories\AttributeFamilyRepository;
use Webkul\Category\Repositories\CategoryRepository;
use Webkul\Core\Contracts\Validations\Slug;
use Webkul\Core\Models\Channel;
use Webkul\Core\Models\Locale;
use Webkul\Inventory\Repositories\InventorySourceRepository;
use Webkul\Product\Helpers\ProductType;
use Webkul\Product\Http\Requests\ProductForm;
use Webkul\Product\Models\Product;
use Webkul\Product\Repositories\ProductAttributeValueRepository;
use Webkul\Product\Repositories\ProductDownloadableLinkRepository;
use Webkul\Product\Repositories\ProductDownloadableSampleRepository;
use Webkul\Product\Repositories\ProductInventoryRepository;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Product\Repositories\ProductFlatRepository;
use Webkul\Admin\DataGrids\CustomerOrdersDataGrid;
use Webkul\Admin\DataGrids\ProductDataGrid;
use Webkul\Admin\DataGrids\ProductLinkedDataGrid;
use Webkul\Admin\DataGrids\ProductLinkedAddDataGrid;
use Webkul\HomeCategory\Repositories\HomeCategoryRepository;
use Webkul\Color\Repositories\ColorRepository;
use Webkul\Attribute\Repositories\AttributeOptionRepository;

class ProductController extends Controller
{
    /**
     * Contains route related configuration.
     *
     * @var array
     */
    protected $_config;


    /**
     * Category repository instance.
     *
     * @var \Webkul\Category\Repositories\CategoryRepository
     */
    protected $categoryRepository;

    /**
     * Home Category repository instance.
     *
     * @var \Webkul\HomeCategory\Repositories\HomeCategoryRepository
     */
    protected $homeCategoryRepository;

    /**
     * Color repository instance.
     *
     * @var \Webkul\Color\Repositories\ColorRepository
     */
    protected $colorRepository;

    /**
     * Product repository instance.
     *
     * @var \Webkul\Product\Repositories\ProductRepository
     */
    protected $productRepository;

    /**
     * Product downloadable link repository instance.
     *
     * @var \Webkul\Product\Repositories\ProductDownloadableLinkRepository
     */
    protected $productDownloadableLinkRepository;

    /**
     * Product downloadable sample repository instance.
     *
     * @var \Webkul\Product\Repositories\ProductDownloadableSampleRepository
     */
    protected $productDownloadableSampleRepository;

    /**
     * Attribute family repository instance.
     *
     * @var \Webkul\Attribute\Repositories\AttributeFamilyRepository
     */
    protected $attributeFamilyRepository;

    /**
     * Inventory source repository instance.
     *
     * @var \Webkul\Inventory\Repositories\InventorySourceRepository
     */
    protected $inventorySourceRepository;

    /**
     * Product attribute value repository instance.
     *
     * @var \Webkul\Product\Repositories\ProductAttributeValueRepository
     */
    protected $productAttributeValueRepository;

    /**
     * Product inventory repository instance.
     *
     * @var \Webkul\Product\Repositories\ProductInventoryRepository
     */
    protected $productInventoryRepository;

    /**
     * attribute options repository instance.
     *
     * @var \Webkul\Attribute\Repositories\AttributeOptionRepository
     */
    protected $attributeOptionRepository;

    protected $productFlatRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Category\Repositories\CategoryRepository                 $categoryRepository
     * @param  \Webkul\Product\Repositories\ProductRepository                   $productRepository
     * @param  \Webkul\Product\Repositories\ProductDownloadableLinkRepository   $productDownloadableLinkRepository
     * @param  \Webkul\Product\Repositories\ProductDownloadableSampleRepository $productDownloadableSampleRepository
     * @param  \Webkul\Attribute\Repositories\AttributeFamilyRepository         $attributeFamilyRepository
     * @param  \Webkul\Inventory\Repositories\InventorySourceRepository         $inventorySourceRepository
     * @param  \Webkul\Product\Repositories\ProductAttributeValueRepository     $productAttributeValueRepository
     * @param  \Webkul\HomeCategory\Repositories\HomeCategoryRepository         $homeCategoryRepository
     * @param  \Webkul\Color\Repositories\ColorRepository         $colorRepository
     * @param  \Webkul\Attribute\Repositories\AttributeOptionRepository         $attributeOptionRepository
     * @return void
     */
    public function __construct(
        CategoryRepository $categoryRepository,
        ProductRepository $productRepository,
        ProductDownloadableLinkRepository $productDownloadableLinkRepository,
        ProductDownloadableSampleRepository $productDownloadableSampleRepository,
        AttributeFamilyRepository $attributeFamilyRepository,
        InventorySourceRepository $inventorySourceRepository,
        ProductAttributeValueRepository $productAttributeValueRepository,
        ProductInventoryRepository $productInventoryRepository,
        ProductFlatRepository $productFlatRepository,
        HomeCategoryRepository $homeCategoryRepository,
        ColorRepository $colorRepository,
        AttributeOptionRepository $attributeOptionRepository
    ) {
        $this->_config = request('_config');

        $this->categoryRepository = $categoryRepository;

        $this->productRepository = $productRepository;

        $this->productDownloadableLinkRepository = $productDownloadableLinkRepository;

        $this->productDownloadableSampleRepository = $productDownloadableSampleRepository;

        $this->attributeFamilyRepository = $attributeFamilyRepository;

        $this->inventorySourceRepository = $inventorySourceRepository;

        $this->productAttributeValueRepository = $productAttributeValueRepository;

        $this->productInventoryRepository = $productInventoryRepository;

        $this->productFlatRepository = $productFlatRepository;

        $this->homeCategoryRepository = $homeCategoryRepository;

        $this->colorRepository = $colorRepository;

        $this->attributeOptionRepository = $attributeOptionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $locale = core()->getRequestedLocaleCode();
        $channel = core()->getRequestedChannelCode();
        if ($channel !== 'all') {
            $channel = Channel::query()->find($channel);
            $channel = $channel ? $channel->code : 'all';
        }

        if ($channel === 'all') {
            $whereInChannels = Channel::query()->pluck('code')->toArray();
        } else {
            $whereInChannels = [$channel];
        }

        if ($locale === 'all') {
            $whereInLocales = Locale::query()->pluck('code')->toArray();
        } else {
            $whereInLocales = [$locale];
        }

        $count = $this->productFlatRepository
            ->whereIn('channel', $whereInChannels)
            ->whereIn('locale', $whereInLocales)
            ->count();
        return view($this->_config['view'], compact('count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $families = $this->attributeFamilyRepository->all();

        $configurableFamily = null;

        if ($familyId = request()->get('family')) {
            $configurableFamily = $this->attributeFamilyRepository->find($familyId);
        }

        return view($this->_config['view'], compact('families', 'configurableFamily'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        if (
            ! request()->get('family')
            && ProductType::hasVariants(request()->input('type'))
            && request()->input('sku') != ''
        ) {
            return redirect(url()->current() . '?type=' . request()->input('type') . '&family=' . request()->input('attribute_family_id') . '&sku=' . request()->input('sku'));
        }

        if (
            ProductType::hasVariants(request()->input('type'))
            && (! request()->has('super_attributes')
                || ! count(request()->get('super_attributes')))
        ) {
            session()->flash('error', trans('admin::app.catalog.products.configurable-error'));

            return back();
        }

        $this->validate(request(), [
            'type'                => 'required',
            'attribute_family_id' => 'required',
            'sku'                 => ['required', 'unique:products,sku', new Slug],
        ]);

        $product = $this->productRepository->create(request()->all());

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Product']));

        return redirect()->route($this->_config['redirect'], ['id' => $product->id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $product = $this->productRepository->with(['variants', 'variants.inventories'])->findOrFail($id);

        $categories = $this->categoryRepository->getCategoryTree();

        $homeCategories = $this->homeCategoryRepository->getCategoryTree();

        $inventorySources = $this->inventorySourceRepository->findWhere(['status' => 1]);

        $colors = $this->colorRepository->get();

        return view($this->_config['view'], compact('product', 'categories', 'inventorySources', 'homeCategories', 'colors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Webkul\Product\Http\Requests\ProductForm  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductForm $request, $id)
    {
        $data = request()->all();

        $multiselectAttributeCodes = array();

        $productAttributes = $this->productRepository->findOrFail($id);

        foreach ($productAttributes->attribute_family->attribute_groups as $attributeGroup) {
            $customAttributes = $productAttributes->getEditableAttributes($attributeGroup);

            if (count($customAttributes)) {
                foreach ($customAttributes as $attribute) {
                    if ($attribute->type == 'multiselect') {
                        array_push($multiselectAttributeCodes, $attribute->code);
                    }
                }
            }
        }

        if (count($multiselectAttributeCodes)) {
            foreach ($multiselectAttributeCodes as $multiselectAttributeCode) {
                if (! isset($data[$multiselectAttributeCode])) {
                    $data[$multiselectAttributeCode] = array();
                }
            }
        }

        $this->productRepository->update($data, $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Product']));

        return redirect()->back();

//        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Update inventories.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateInventories($id)
    {
        $product = $this->productRepository->findOrFail($id);

        $this->productInventoryRepository->saveInventories(request()->all(), $product);

        return response()->json([
            'message' => __('admin::app.catalog.products.saved-inventory-message'),
            'updatedTotal' => $this->productInventoryRepository->where('product_id', $product->id)->sum('qty')
        ]);
    }

    /**
     * Uploads downloadable file.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadLink($id)
    {
        return response()->json(
            $this->productDownloadableLinkRepository->upload(request()->all(), $id)
        );
    }

    /**
     * Copy a given Product.
     *
     * @param  int  $productId
     * @return \Illuminate\Http\Response
     */
    public function copy(int $productId)
    {
        $originalProduct = $this->productRepository->findOrFail($productId);

        if (! $originalProduct->getTypeInstance()->canBeCopied()) {
            session()->flash(
                'error',
                trans('admin::app.response.product-can-not-be-copied', [
                    'type' => $originalProduct->type,
                ])
            );

            return redirect()->to(route('admin.catalog.products.index'));
        }

        if ($originalProduct->parent_id) {
            session()->flash(
                'error',
                trans('admin::app.catalog.products.variant-already-exist-message')
            );

            return redirect()->to(route('admin.catalog.products.index'));
        }

        $copiedProduct = $this->productRepository->copy($originalProduct);

        if ($copiedProduct instanceof Product && $copiedProduct->id) {
            session()->flash('success', trans('admin::app.response.product-copied'));
        } else {
            session()->flash('error', trans('admin::app.response.error-while-copying'));
        }

        return redirect()->to(route('admin.catalog.products.edit', ['id' => $copiedProduct->id]));
    }

    /**
     * Uploads downloadable sample file.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uploadSample($id)
    {
        return response()->json(
            $this->productDownloadableSampleRepository->upload(request()->all(), $id)
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->productRepository->findOrFail($id);

        try {
            $this->productRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Product']));

            return response()->json(['message' => true], 200);
        } catch (Exception $e) {
            report($e);

            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Product']));
        }

        return response()->json(['message' => false], 400);
    }

    /**
     * Mass Delete the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function massDestroy()
    {
        $productIds = explode(',', request()->input('indexes'));

        foreach ($productIds as $productId) {
            $product = $this->productRepository->find($productId);

            if (isset($product)) {
                $this->productRepository->delete($productId);
            }
        }

        session()->flash('success', trans('admin::app.catalog.products.mass-delete-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Mass updates the products.
     *
     * @return \Illuminate\Http\Response
     */
    public function massUpdate()
    {
        $data = request()->all();

        if (! isset($data['massaction-type'])) {
            return redirect()->back();
        }

        if (! $data['massaction-type'] == 'update') {
            return redirect()->back();
        }

        $productIds = explode(',', $data['indexes']);

        foreach ($productIds as $productId) {
            $this->productRepository->update([
                'channel' => null,
                'locale'  => null,
                'status'  => $data['update-options'],
            ], $productId);
        }

        session()->flash('success', trans('admin::app.catalog.products.mass-update-success'));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * To be manually invoked when data is seeded into products.
     *
     * @return \Illuminate\Http\Response
     */
    public function sync()
    {
        Event::dispatch('products.datagrid.sync', true);

        return redirect()->route('admin.catalog.products.index');
    }

    /**
     * Result of search product.
     *
     * @return \Illuminate\View\View|\Illuminate\Http\Response
     */
    public function productLinkSearch()
    {
        if (request()->ajax()) {
            $results = [];

            foreach ($this->productRepository->searchProductByAttribute(request()->input('query')) as $row) {
                $results[] = [
                    'id'   => $row->product_id,
                    'sku'  => $row->sku,
                    'name' => $row->name,
                ];
            }

            return response()->json($results);
        } else {
            return view($this->_config['view']);
        }
    }

    /**
     * Download image or file.
     *
     * @param  int  $productId
     * @param  int  $attributeId
     * @return \Illuminate\Http\Response
     */
    public function download($productId, $attributeId)
    {
        $productAttribute = $this->productAttributeValueRepository->findOneWhere([
            'product_id'   => $productId,
            'attribute_id' => $attributeId,
        ]);

        return Storage::download($productAttribute['text_value']);
    }

    /**
     * Search simple products.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchSimpleProducts()
    {
        return response()->json(
            $this->productRepository->searchSimpleProducts(request()->input('query'))
        );
    }

    public function grid()
    {
        if (request()->ajax()) {
            return app(ProductAjaxDataGrid::class)->toJson();
        }

        return view($this->_config['view']);
    }

    public function linkedGrid()
    {
        if (request()->ajax()) {
            return app(ProductLinkedDataGrid::class)->toJson();
        }

        return view($this->_config['view']);
    }

    public function linkedGridAdd()
    {
        if (request()->ajax()) {
            return app(ProductLinkedAddDataGrid::class)->toJson();
        }

        return view($this->_config['view']);
    }


    public function customerOrders($id)
    {
        if (request()->ajax()) {
            return app(CustomerOrdersDataGrid::class)->toJson();
        }

        return view($this->_config['view']);
    }


    public function removeLinkedProducts($productId, $key)
    {
        $data = request()->all();
        $productIds = explode(',', $data['indexes']);
        $productsType = $key;
        $product = $this->productRepository->find($productId);

        $product->$productsType()->wherePivotIn('child_id', $productIds)->detach();

        return response()->json(['message' => 'Связь между продуктами успешно удалена'], 200);
    }

    public function addLinkedProducts($productId, $key)
    {
        $productId = $productId;
        $data = request()->all();
        $productsType = $key;

        $product = $this->productRepository->findOrFail($productId);

        $alreadyLinkedProducts = $product->$productsType()->get()->pluck('id')->toArray();

        $productIds = explode(',', $data['indexes']);
        $productIds = array_map('intval', $productIds);
        $resultArray = array_merge($productIds, $alreadyLinkedProducts);
        $resultArray = array_unique($resultArray);

        $product->$productsType()->sync($resultArray ?? []);

        return response()->json(['message' => 'Продукты успешно связаны'], 200);
    }

    public function sortLinkedProds($id, $productsType) {
        $product = $this->productRepository->findOrFail($id);
        try {
            $items = request()->all();
            foreach ($items as $key => $item) {
                $product->$productsType()->wherePivot('child_id', $item['product_id'])->update([
                    'sort' => $item['index'],
                ]);
            }
            return response()->json(['message' => 'Сортировка продуктов успешно изменена'], 200);
        } catch (Exception $e) {
            report($e);
            return response()->json(['message' => 'При сортировке продуктов возникла ошибка'], 400);
        }
    }

    public function sortImages($id) {
        $product = $this->productRepository->findOrFail($id);
        $items = request()->all();
        foreach ($items as $item) {
            $product->images()->where('id', (int)$item['image_id'])->update([
                'sort' => $item['index'],
            ]);
        }
        return response()->json(['message' => 'Сортировка изображений успешно изменена'], 200);
    }

    public function import() {

        $sizeAttribute = DB::table('attributes')->select(['id'])->where([
            'code'  => 'size',
        ])->first();

        $sizeAttribute = $sizeAttribute->id;

        $json = '
                {
            "items": [
                {
                    "id": "61edd415-011c-4065-8edf-5c139a29755f",
                    "sku": "article488green",
                    "title": "Шапка зелёная",
                    "size": "S",
                    "balance": 20,
                    "weight": 6,
                    "basePrice": 150000,
                    "description": "Описание 1",
                    "care": "61edd415-742c-4065-8edf-5c139a29755f",
                    "textile": "61ere419-742c-4065-8edf-5c139a20755f",
                    "components": "61ere415-742c-4065-8edf-5c139a29755f",
                    "filterLevelOne": "61edf419-742c-4065-1edf-5c139a20755f",
                    "filterLevelTwo": "61ed6412-7t2c-4360-8edf-5c132a29755f",
                    "color": "61edf410-755c-4005-1edf-5c139a20750f"
                },
                {
                    "id": "61edd415-012c-4065-8edf-5c139a29755a",
                    "sku": "article488green",
                    "title": "Шапка зелёная",
                    "size": "XXL",
                    "balance": 600,
                    "weight": 15,
                    "basePrice": 206000,
                    "description": "Описание 1",
                    "care": "61edd415-742c-4065-8edf-5c139a29755f",
                    "textile": "61ere419-742c-4065-8edf-5c139a20755f",
                    "components": "61ere415-742c-4065-8edf-5c139a29755f",
                    "filterLevelOne": "61edf419-742c-4065-1edf-5c139a20755f",
                    "filterLevelTwo": "61ed6412-7t2c-4360-8edf-5c132a29755f",
                    "color": "61edf410-755c-4005-1edf-5c139a20750f"
                },
                                {
                    "id": "61edd415-013c-4065-8edf-5c139a29755f",
                    "sku": "article488blue",
                    "title": "Шапка синяя",
                    "size": "S",
                    "balance": 20,
                    "weight": 6,
                    "basePrice": 150000,
                    "description": "Описание 1",
                    "care": "61edd415-742c-4065-8edf-5c139a29755f",
                    "textile": "61ere419-742c-4065-8edf-5c139a20755f",
                    "components": "61ere415-742c-4065-8edf-5c139a29755f",
                    "filterLevelOne": "61edf419-742c-4065-1edf-5c139a20755f",
                    "filterLevelTwo": "61ed6412-7t2c-4360-8edf-5c132a29755f",
                    "color": "61edf410-755c-4065-1ede-5z139a20755f"
                },
                {
                    "id": "61edd415-014c-4065-8edf-5c139a29755a",
                    "sku": "article488blue",
                    "title": "Шапка синяя",
                    "size": "XXL",
                    "balance": 600,
                    "weight": 15,
                    "basePrice": 206000,
                    "description": "Описание 1",
                    "care": "61edd415-742c-4065-8edf-5c139a29755f",
                    "textile": "61ere419-742c-4065-8edf-5c139a20755f",
                    "components": "61ere415-742c-4065-8edf-5c139a29755f",
                    "filterLevelOne": "61edf419-742c-4065-1edf-5c139a20755f",
                    "filterLevelTwo": "61ed6412-7t2c-4360-8edf-5c132a29755f",
                    "color": "61edf410-755c-4065-1ede-5z139a20755f"
                },
                {
                    "id": "61edd415-014r-4065-8rdf-5c139a29755a",
                    "sku": "article488red",
                    "title": "Шапка красная",
                    "size": "60",
                    "balance": 600,
                    "weight": 15,
                    "basePrice": 206000,
                    "description": "Описание красной шапки",
                    "care": "61edd415-742c-4065-8edf-5c139a29755f",
                    "textile": "61ere419-742c-4065-8edf-5c139a20755f",
                    "components": "61ere415-742c-4065-8edf-5c139a29755f",
                    "filterLevelOne": "61edf419-742c-4065-1edf-5c139a20755f",
                    "filterLevelTwo": "61ed6412-7t2c-4360-8edf-5c132a29755f",
                    "color": "61edf419-755c-4065-1edf-5c139a20755f"
                }
            ],
            "limit": 1,
            "offset": 1,
            "total": 1
        }
        ';
        $data = json_decode($json);
        $items = $data->items;
        $collection = new Collection();
        foreach($items as $item){
            $collection->push((object)$item);
        }

        $importData = [
            "type" => 'configurable',
            "attribute_family_id" => 2,
            "sku" => null,
            "family" => 2,
            "view_type_id" => null,
            "color_id" => null,
            'inventories' => [1 => 0],
            "main_prod_attributes" => [
                "update" => false,
                "name" => "",
                "description" => "",
                "care" => "",
                "textile" => "",
                "composition" => "",
            ],
            "super_attributes" => [
                "size" => [
                ],
            ],
            "size_prod_attributes" => [
            ],
        ];

        $productsBySku = $collection->groupBy('sku');

        foreach ($productsBySku as $key => $items) {
            $productAllSizesCount = 0;
            $importData['super_attributes']['size'] = [];
            $importData['size_prod_attributes'] = [];

            $product = $this->productRepository->findWhere(['sku' => $key])->first();
            if ($product) {
//                $importData['main_prod_attributes']['update'] = true;
                continue;
            }
            print_r("Продукты с sku = " . $key . ':');
            $sku = $key;
            $title = $items[0]->title;
            $importData['sku'] = $sku;
            $importData['main_prod_attributes']['name'] = $title;
            $importData['main_prod_attributes']['description'] = $items[0]->description;

            //Ищем в БД раннее импортированный из 1С справочник - Уход
            $manual = DB::table('care_manuals')->select(['value'])->where([
                '1c_id'  => $items[0]->care,
            ])->first();
            if ($manual) {
                $importData['main_prod_attributes']['care'] = $manual->value;
            }else{
                //Может быть нужно будет запускать импорт справочников из 1С
            }

            //Ищем в БД раннее импортированный из 1С справочник - Ткань
            $manual = DB::table('textile_manuals')->select(['value'])->where([
                '1c_id'  => $items[0]->textile,
            ])->first();
            if ($manual) {
                $importData['main_prod_attributes']['textile'] = $manual->value;
            }else{
                //Может быть нужно будет запускать импорт справочников из 1С
            }

            //Ищем в БД раннее импортированный из 1С справочник - Состав
            $manual = DB::table('components_manuals')->select(['value'])->where([
                '1c_id'  => $items[0]->components,
            ])->first();
            if ($manual) {
                $importData['main_prod_attributes']['composition'] = $manual->value;
            }else{
                //Может быть нужно будет запускать импорт справочников из 1С
            }

            //Ищем в БД раннее импортированный из 1С справочник - фильтр первого или второго уровня
            if (!empty($items[0]->filterLevelTwo)){
                $filter1CId = $items[0]->filterLevelTwo;
            }
            $filter1CId = !empty($items[0]->filterLevelTwo) ? $items[0]->filterLevelTwo : $items[0]->filterLevelOne;
            if ($filter1CId){
                $manual = DB::table('view_types')->select(['id'])->where([
                    '1c_id'  => $filter1CId,
                ])->first();
                if ($manual) {
                    $importData['view_type_id'] = $manual->id;
                }
            }

            //Ищем в БД раннее импортированный из 1С справочник - цвет
            $manual = DB::table('colors')->select(['id'])->where([
                '1c_id'  => $items[0]->color,
            ])->first();
            if ($manual) {
                $importData['color_id'] = $manual->id;
            }

            foreach ($items as $item) {
                //Ищем в БД размер - если есть, то создадим вариант по нему, если нет, то добавим размер из 1С в базу
                $sizeAttributeOption = $this->attributeOptionRepository->findOneWhere(['attribute_id' => $sizeAttribute, 'admin_name' => $item->size]);
                if ($sizeAttributeOption) {
                    $importData['super_attributes']['size'][] = $sizeAttributeOption->id;
                }else{
                    $sizeAttributeOption = $this->attributeOptionRepository->create(['admin_name' => $item->size, 'attribute_id' => $sizeAttribute]);
                    $importData['super_attributes']['size'][] = $sizeAttributeOption->id;
                }
                $productAllSizesCount += $item->balance;
                $importData['size_prod_attributes'][] = [
                    "price" => round($item->basePrice / 100),
                    "special_price" => 1,
                    "status" => 1,
                    "weight" => $item->weight,
                    "name" => $item->title,
                    'inventories' => [1 => $item->balance],
                    "1c_id" => $item->id,
                ];
//                dump($item);
            }
            $importData['inventories'][1] = $productAllSizesCount;
            $product = $this->productRepository->createFromImport($importData);

            dump($product);

            print_r("Варианты: ");

            dump($product->variants);
        }
    }

}
