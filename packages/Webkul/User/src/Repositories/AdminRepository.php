<?php

namespace Webkul\User\Repositories;

use Illuminate\Support\Facades\Storage;
use Webkul\Core\Eloquent\Repository;

class AdminRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\User\Contracts\Admin';
    }


    public function create(array $data)
    {
        $admin = $this->model->create($data);

        $this->uploadImages($data, $admin);

        return $admin;
    }

    public function update(array $data, $id, $attribute = "id")
    {
        $admin = $this->find($id);

        $admin->update($data);

        $this->uploadImages($data, $admin);

        return $admin;
    }

    public function uploadImages($data, $admin, $type = "image")
    {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'admin/' . $admin->id;

                if ($request->hasFile($file)) {
                    if ($admin->{$type}) {
                        Storage::delete($admin->{$type});
                    }

                    $admin->{$type} = $request->file($file)->store($dir);
                    $admin->save();
                }
            }
        } else {
            if ($admin->{$type}) {
                Storage::delete($admin->{$type});
            }

            $admin->{$type} = null;
            $admin->save();
        }
    }
}