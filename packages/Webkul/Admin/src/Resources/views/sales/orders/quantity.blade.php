<span id="product-{{ $product->id }}-quantity">
    <a id="product-{{ $product->id }}-quantity-anchor" href="javascript:void(0);" onclick="showEditQuantityForm('{{ $product->id }}')">{{ $qty }}</a>
</span>

<span id="edit-product-{{ $product->id }}-quantity-form-block" style="display: none;">
    <form id="edit-product-{{ $product->id }}-quantity-form" action="javascript:void(0);">
        @csrf

        @method('PUT')

        <div class="control-group" :class="[errors.has('quantity') ? 'has-error' : '']">
            <label>Количество</label>

            <input type="text" v-validate="'numeric|min:0'" name="quantity" class="control" value="{{ $qty }}" data-vv-as="&quot;Количество&quot;"/>
            <input type="hidden" name="order_id" value="{{ $id }}">
            <input type="hidden" name="type" value="{{ $type }}">

            <span class="control-error" v-if="errors.has('quantity')">@{{ errors.first('quantity') }}</span>
        </div>

        <button class="btn btn-primary" onclick="saveEditQuantityForm('{{ route('admin.sales.orders.update-quantity', $product->id) }}', '{{ $product->id }}')">{{ __('admin::app.catalog.products.save') }}</button>
        <button class="btn btn-danger" onclick="cancelEditQuantityForm('{{ $product->id }}')">{{ __('admin::app.catalog.products.cancel') }}</button>
    </form>
</span>