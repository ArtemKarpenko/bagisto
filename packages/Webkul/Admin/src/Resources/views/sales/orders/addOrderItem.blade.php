<modal class="extended_modal" id="productsTable" :is-open="modalIds.productsTable">
    <h3 slot="header">Добавить продукты</h3>
    <div slot="body">
        <datagrid-plus type="orderProducts" src="{{ route('admin.catalog.products.grid', $order->id) }}">
        </datagrid-plus>
    </div>
</modal>

<div class="btn btn-primary btn-lg mb-20" @click="showModal('productsTable')">Добавить продукты</div>

{{--<search-product></search-product>--}}

@push('scripts')

    <script type="text/x-template" id="search-products-template">
        <div class="control-group">
            <form action="{{ route('admin.sales.orders.add-item', $order->id) }}" method="POST">
                @csrf()
                <input type="text" class="control" autocomplete="off" v-model="search_value" placeholder="{{ __('admin::app.catalog.products.product-search-hint') }}" v-on:keyup="search()">

                <div class="linked-product-search-result">
                    <ul>
                        <li v-for='(product, index) in products' v-if='products.length' @click="addProductToOrder($event, product)">
                            @{{ product.name }}
                        </li>

    {{--                    <li v-if='! products.length && ! is_searching'>--}}
    {{--                        {{ __('admin::app.catalog.products.no-result-found') }}--}}
    {{--                    </li>--}}

                        <li v-if="is_searching">
                            {{ __('admin::app.catalog.products.searching') }}
                        </li>
                    </ul>
                </div>
            </form>
        </div>
    </script>

    <script>

        Vue.component('search-product', {

            template: '#search-products-template',

            data: function() {
                return {
                    search_value: '',
                    products: [],
                    is_searching: false,
                }
            },

            created: function () {

            },

            methods: {
                addProductToOrder: function ($e, product) {
                    let element = $e.target;
                    let form = $(element).closest('form');
                    let token = $(form).find('input[name="_token"]').val();

                    this.$http.post ("{{ route('admin.sales.orders.add-item', $order->id) }}", {token: token, product_id: product.id})
                        .then (function(response) {
                            location.reload();
                        })
                },

                search: function () {
                    this_this = this;

                    this.is_searching = true

                    if(this.search_value.length >= 1 ) {
                        this.$http.get ("{{ route('admin.catalog.products.productlinksearch') }}", {params: {query: this.search_value}})
                            .then (function(response) {
                                for (var index in response.data) {
                                    if (response.data[index].id == this_this.productId) {
                                        response.data.splice(index, 1);
                                    }
                                }
                                this_this.products = response.data;
                                this_this.is_searching = false;
                            })
                            .catch (function (error) {
                                this_this.is_searching = false;
                            })
                    }else{
                        this_this.products = [];
                        this_this.is_searching = false;
                    }
                }
            }
        });

    </script>

@endpush