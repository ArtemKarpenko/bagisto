@if(isset ($type) && $type === 'shipping_address')

    <address-form></address-form>


    @push('scripts')

        <script type="text/x-template" id="address-form-template">
            <div>
                Клиент: <b>{{ $address->name }}</b><br><br>
                <form action="{{ route('admin.sales.orders.address.update', $id) }}" v-on:submit.prevent="onSubmitAddress">
                    @csrf

                    @method('PUT')

                    <div class="control-group" :class="[errors.has('company_name') ? 'has-error' : '']">
                        <label for="company_name">
                            Название компании
                        </label>
                        <input type="text" class="control" name="company_name" value="{{ old('company_name') ?: $address->company_name }}" data-vv-as="&quot;Название компонии&quot;">
                        <span class="control-error" v-if="errors.has('company_name')">@{{ errors.first('company_name') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('address1') ? 'has-error' : '']">
                        <label for="address1" class="required">
                            Улица
                        </label>
                        <input type="text" class="control" name="address1" v-validate="'required'" value="{{ old('address1') ?: $address->address1 }}" data-vv-as="&quot;Улица&quot;">
                        <span class="control-error" v-if="errors.has('address1')">@{{ errors.first('address1') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('postcode') ? 'has-error' : '']">
                        <label for="postcode" class="required">
                            Почтовый индекс
                        </label>
                        <input type="text" class="control" name="postcode" v-validate="'required'" value="{{ old('postcode') ?: $address->postcode }}" data-vv-as="&quot;Почтовый индекс&quot;">
                        <span class="control-error" v-if="errors.has('postcode')">@{{ errors.first('postcode') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('city') ? 'has-error' : '']">
                        <label for="city" class="required">
                            Город
                        </label>
                        <input type="text" class="control" name="city" v-validate="'required'" value="{{ old('city') ?: $address->city }}" data-vv-as="&quot;Город&quot;">
                        <span class="control-error" v-if="errors.has('city')">@{{ errors.first('city') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('state') ? 'has-error' : '']">
                        <label for="state" class="required">
                            Штат
                        </label>
                        <input type="text" class="control" name="state" v-validate="'required'" value="{{ old('state') ?: $address->state }}" data-vv-as="&quot;Штат&quot;">
                        <span class="control-error" v-if="errors.has('state')">@{{ errors.first('state') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('country') ? 'has-error' : '']">
                        <label for="country" class="required">Страна</label>
                        <select name="country" class="control" id="country" value="{{ $address->country }}" v-validate="'required'" data-vv-as="&quot;Страна&quot;">
                            <option value=""></option>
                            @foreach (core()->countries() as $country)
                                <option @if($country->code == $address->country) selected @endif value="{{ $country->code }}">{{ $country->name }}</option>
                            @endforeach
                        </select>
                        <span class="control-error" v-if="errors.has('country')">@{{ errors.first('country') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('phone') ? 'has-error' : '']">
                        <label for="phone" class="required">
                            Контакт
                        </label>
                        <input type="text" class="control" name="phone" v-validate="'required'" value="{{ old('phone') ?: $address->phone }}" data-vv-as="&quot;Контакт&quot;">
                        <span class="control-error" v-if="errors.has('phone')">@{{ errors.first('phone') }}</span>
                    </div>

                    <button type="submit" class="btn btn-lg btn-primary">
                        Сохранить адрес
                    </button>
                </form>
            </div>
        </script>

        <script>

            Vue.component('address-form', {

                template: '#address-form-template',

                data: function() {
                    return {

                    }
                },

                created: function () {

                },

                methods: {
                    onSubmitAddress(event) {
                        let self = this;
                        let form = event.target;
                        let url = $(form).attr('action');
                        let array = $(form).serialize();
                        axios
                            .post(url, array)
                            .then(function (response) {
                                window.flashMessages = [{'type': 'alert-success', 'message': response.data.message }];
                                self.$root.addFlashMessages();
                            }).catch(
                            function (error) {
                                window.flashMessages = [{'type': 'alert-error', 'message': error.response.data.message }];
                                self.$root.addFlashMessages();
                            }
                        );
                    }
                }
            });

        </script>

    @endpush
@else
    {{ $address->company_name ?? '' }}<br>
    <b>{{ $address->name }}</b><br>
    {{ $address->address1 }}<br>
    {{ $address->postcode }} {{ $address->city }}<br>
    {{ $address->state }}<br>
    {{ core()->country_name($address->country) }}<br></br>
    {{ __('shop::app.checkout.onepage.contact') }} : {{ $address->phone }}
@endif