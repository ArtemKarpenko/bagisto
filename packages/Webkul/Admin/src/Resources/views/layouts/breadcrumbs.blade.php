@if (isset($links) && count($links))
    <div>
        @foreach ($links as $url => $title)
            @if ($url)
                <span class="top-navigation__link breadcrumbs_link breadcrumbs_link_wrapper">
              <a href="{{ $url }}" class="top-navigation__link breadcrumbs_link">
                  <span>{{ $title }}</span>
              </a>
            </span>
            @else
                <span class="top-navigation__link breadcrumbs_link breadcrumbs_link_wrapper">
              <span>
                  <span>{{ $title }}</span>
              </span>
            </span>
            @endif
        @endforeach
    </div>
@endif
