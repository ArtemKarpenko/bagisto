{!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.images.before', ['product' => $product]) !!}

<accordian :title="'{{ __('admin::app.catalog.products.images') }}'" :active="false">
    <div slot="body">

        {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.images.controls.before', ['product' => $product]) !!}

        <div class="control-group full-width {!! $errors->has('images.*') ? 'has-error' : '' !!}">
            <label>{{ __('admin::app.catalog.categories.image') }}</label>

            <product-image></product-image>

            <span class="control-error" v-if="{!! $errors->has('images.*') !!}">
                @php $count=1 @endphp
                @foreach ($errors->get('images.*') as $key => $message)
                    @php echo str_replace($key, 'Image'.$count, $message[0]); $count++ @endphp
                @endforeach
            </span>
        </div>

        {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.images.controls.after', ['product' => $product]) !!}

    </div>
</accordian>

{!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.images.after', ['product' => $product]) !!}

@push('scripts')
    @parent

    <script type="text/x-template" id="product-image-template">
        <div>
{{--            <div class="image-wrapper">--}}
{{--            @{{ items }}--}}
                <draggable
                    :list="items"
                    class="image-wrapper"
                    v-bind="dragOptions"
                    ghost-class="ghost"
                    @change="dropItem"
                >
                    <product-image-item
                        v-for='(image, index) in items'
                        :key='image.id'
                        :image="image"
                        :index="index"
                        @onRemoveImage="removeImage($event)"
                        @onImageSelected="imageSelected($event)"
                        @onChangeImageParameters="changeImageParameters"
                    ></product-image-item>
                </draggable>
{{--            </div>--}}

            <label class="btn btn-lg btn-primary" style="display: inline-block; width: auto" @click="createFileType">
                {{ __('admin::app.catalog.products.add-image-btn-title') }}
            </label>
        </div>
    </script>

    <script type="text/x-template" id="product-image-item-template">
        <label class="image-item" v-bind:class="{ 'has-image': imageData.length > 0 }">

            <label>
                <div class="dropdown-dots-wrap">
                    <div class="dropdown-toggle">
                        <div class="dropdown-dots">
                            ...
                        </div>
                    </div>

                    <div
                        class="dropdown-list dropdown-container"
                        style="display: none"
                    >
                        <div>
                            <ul>
                                <li @click="changeImageParameters('isMain', index)"><div>Основное фото <i v-if="image.parameters.isMain == 1" class="icon check-accent"></i></div></li>
                                <li @click="changeImageParameters('isCatalog', index)"><div>Фото в каталоге <i v-if="image.parameters.isCatalog == 1" class="icon check-accent"></i></div></li>
                                <li @click="changeImageParameters('isThing', index)"><div>Предметка <i v-if="image.parameters.isThing == 1" class="icon check-accent"></i></div></li>
                                <li @click="reChange($event)"><div>Заменить фото</div></li>
                                <li @click="removeImage()"><div class="delete">Удалить фото</div></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </label>

            <input type="hidden" :name="'images[' + image.id + ']'" v-if="! new_image"/>

            <div v-if="image.path">
                <input type="hidden" :name="`imageParameters[old][${image.id}][isMain]`" v-model="image.parameters.isMain" />
                <input type="hidden" :name="`imageParameters[old][${image.id}][isCatalog]`" v-model="image.parameters.isCatalog" />
                <input type="hidden" :name="`imageParameters[old][${image.id}][isThing]`" v-model="image.parameters.isThing" />
                <input type="hidden" :name="`imageParameters[old][${image.id}][sort]`" v-model="image.sort" />
            </div>
            <div v-else>
                <input type="hidden" :name="`imageParameters[new][${image.currentImageCount}][isMain]`" v-model="image.parameters.isMain" />
                <input type="hidden" :name="`imageParameters[new][${image.currentImageCount}][isCatalog]`" v-model="image.parameters.isCatalog" />
                <input type="hidden" :name="`imageParameters[new][${image.currentImageCount}][isThing]`" v-model="image.parameters.isThing" />
                <input type="hidden" :name="`imageParameters[new][${image.currentImageCount}][sort]`" v-model="image.sort" />
            </div>

{{--            <label style="position: absolute;" v-text="image"></label>--}}
{{--            <input type="file" v-validate="'mimes:image/*'" accept="image/*" :name="'images[]'" ref="imageInput" :id="_uid" @change="addImageView($event)" multiple="multiple"/>--}}

            <input v-if="image.path" type="file" v-validate="'mimes:image/*'" accept="image/*" :name="'images[]'" ref="imageInput" :id="_uid" @change="addImageView($event)" multiple="multiple"/>
            <input v-else type="file" v-validate="'mimes:image/*'" accept="image/*" :name="`images[${image.currentImageCount}]`" ref="imageInput" :id="_uid" @change="addImageView($event)" multiple="multiple"/>

            <img class="preview" :src="imageData" v-if="imageData.length > 0">

            <label class="remove-image" @click="removeImage()">
                {{ __('admin::app.catalog.products.remove-image-btn-title') }}
            </label>
        </label>
    </script>

    <script>
        Vue.component('product-image', {

            template: '#product-image-template',

            data: function() {
                return {
                    images: @json($product->images),

                    imageCount: 0,

                    items: []
                }
            },

            components: {
                // draggable
            },

            computed: {
                finalInputName: function() {
                    return 'images[' + this.image.id + ']';
                },

                dragOptions() {
                    return {
                        animation: 200,
                        group: "description",
                        disabled: false,
                        ghostClass: "ghost",
                        activeForm: null,
                    };
                },
            },

            created: function() {
                var this_this = this;

                this.images.forEach(function(image) {
                    if (image.parameters === null) {
                        image.parameters = {'isMain' : 0, 'isCatalog': 0, 'isThing': 0};
                    }
                    this_this.items.push(image)

                    this_this.imageCount++;
                });
            },

            methods: {
                dropItem() {
                    let self = this;
                    let count = 1;
                    let images = [];
                    for (let value of this.items) {
                        if (value.id) {
                            images.push({'index': count, 'image_id': value.id});
                            value.sort = count;
                            count = count + 1;
                        }
                    }

                    {{--axios--}}
                    {{--    .post("{{ route('admin.catalog.products.edit.sort-images', $product->id) }}", images)--}}
                    {{--    .then(function (response) {--}}
                    {{--        window.flashMessages = [{'type': 'alert-success', 'message': response.data.message }];--}}
                    {{--        self.$root.addFlashMessages();--}}
                    {{--    }).catch(function (error) {--}}
                    {{--    window.flashMessages = [{'type': 'alert-error', 'message': error.response.data.message }];--}}
                    {{--    self.$root.addFlashMessages();--}}
                    {{--});--}}
                },

                createFileType: function() {
                    var this_this = this;

                    this.imageCount++;

                    this.items.push({'id': 'image_' + this.imageCount, currentImageCount: this.imageCount, parameters: {'isMain' : 0, 'isCatalog': 0, 'isThing': 0}, sort: this.imageCount});
                },

                changeImageParameters(data) {
                    if(this.items[data.key].parameters[data.type] == 0){
                        if(data.type === 'isMain') {
                            for (let value of this.items) {
                                value.parameters.isMain = 0;
                            }
                        }
                        this.items[data.key].parameters[data.type] = 1;
                    }else{
                        this.items[data.key].parameters[data.type] = 0;
                    }
                },

                removeImage (image) {
                    let index = this.items.indexOf(image)

                    this.imageCount--;

                    Vue.delete(this.items, index);
                },

                imageSelected: function(event) {
                    var this_this = this;

                    Array.from(event.files).forEach(function(image, index) {
                        if (index) {
                            this_this.imageCount++;

                            this_this.items.push({'id': 'image_' + this_this.imageCount, file: image, currentImageCount: this_this.imageCount, parameters: {'isMain' : 0, 'isCatalog': 0, 'isThing': 0}, sort: this_this.imageCount});
                        }
                    });
                }
            }
        });

        Vue.component('product-image-item', {

            template: '#product-image-item-template',

            props: {
                image: {
                    type: Object,
                    required: false,
                    default: null
                },
                index: {
                    type: Number,
                    required: false,
                    default: 0
                },
            },

            data: function() {
                return {
                    imageData: '',

                    new_image: 0
                }
            },

            mounted () {
                if (this.image.id && this.image.url) {
                    this.imageData = this.image.url;
                } else if (this.image.id && this.image.file) {
                    this.readFile(this.image.file);
                }
            },

            computed: {
                finalInputName: function() {
                    return this.inputName + '[' + this.image.id + ']';
                }
            },

            methods: {
                changeImageParameters: function(type, key) {
                    this.$emit('onChangeImageParameters', {type, key})
                },

                reChange(event) {
                    $(event.target).closest('.image-item').find('img').click();
                },

                addImageView: function() {
                    var imageInput = this.$refs.imageInput;

                    if (imageInput.files && imageInput.files[0]) {
                        if (imageInput.files[0].type.includes('image/')) {
                            this.readFile(imageInput.files[0])

                            if (imageInput.files.length > 1) {
                                this.$emit('onImageSelected', imageInput)
                            }
                        } else {
                            imageInput.value = "";

                            alert('Only images (.jpeg, .jpg, .png, ..) are allowed.');
                        }
                    }
                },

                readFile: function(image) {
                    var reader = new FileReader();

                    reader.onload = (e) => {
                        this.imageData = e.target.result;
                    }

                    reader.readAsDataURL(image);

                    this.new_image = 1;
                },

                removeImage: function() {
                    this.$emit('onRemoveImage', this.image)
                }
            }
        });
    </script>

@endpush
