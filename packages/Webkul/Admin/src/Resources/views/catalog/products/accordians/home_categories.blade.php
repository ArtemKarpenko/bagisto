@if ($homeCategories->count())

    {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.before', ['product' => $product]) !!}

    <accordian :title="'Категории новые'" :active="false">
        <div slot="body">

            {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.controls.before', ['product' => $product]) !!}

            <tree-view behavior="normal" value-field="id" name-field="home_categories" input-type="checkbox" items='@json($homeCategories)' value='@json($product->homeCategories->pluck("id"))' fallback-locale="{{ config('app.fallback_locale') }}"></tree-view>

            {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.controls.after', ['product' => $product]) !!}

        </div>
    </accordian>

    {!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.categories.after', ['product' => $product]) !!}

@endif