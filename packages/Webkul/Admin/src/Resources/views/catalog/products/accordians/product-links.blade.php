{!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.product_links.before', ['product' => $product]) !!}

<accordian :title="'{{ __('admin::app.catalog.products.product-link') }}'" :active="false">
    <div slot="body">

        <linked-products></linked-products>

    </div>
</accordian>

{!! view_render_event('bagisto.admin.catalog.product.edit_form_accordian.product_links.before', ['product' => $product]) !!}

@push('scripts')

<script type="text/x-template" id="linked-products-template">
    <div>

        <modal class="extended_modal" id="productsTable" :is-open="$root.modalIds.productsTable">
            <h3 slot="header">Добавить продукты</h3>
            <div slot="body">
                <datagrid-plus v-if="(productsType == 'up_sells')" type="linkedProductsAdd" src="{{ route('admin.catalog.products.linkedGrid.add', [$product->id, 'up_sells']) }}" :productsType="productsType" :updateGrid="updateGrid">
                </datagrid-plus>
                <datagrid-plus v-if="(productsType == 'cross_sells')" type="linkedProductsAdd" src="{{ route('admin.catalog.products.linkedGrid.add', [$product->id, 'cross_sells']) }}" :productsType="productsType" :updateGrid="updateGrid">
                </datagrid-plus>
                <datagrid-plus v-if="(productsType == 'related_products')" type="linkedProductsAdd" src="{{ route('admin.catalog.products.linkedGrid.add', [$product->id, 'related_products']) }}" :productsType="productsType" :updateGrid="updateGrid">
                </datagrid-plus>
                <datagrid-plus v-if="(productsType == 'similar_colors')" type="linkedProductsAdd" src="{{ route('admin.catalog.products.linkedGrid.add', [$product->id, 'similar_colors']) }}" :productsType="productsType" :updateGrid="updateGrid">
                </datagrid-plus>
            </div>
        </modal>

        <div class="control-group" v-for='(key) in linkedProducts'>
            <label for="up-selling" v-if="(key == 'up_sells')">
                {{ __('admin::app.catalog.products.up-selling') }}
            </label>
            <label for="cross_sells" v-if="(key == 'cross_sells')">
                {{ __('admin::app.catalog.products.cross-selling') }}
            </label>
            <label for="related_products" v-if="(key == 'related_products')">
                {{ __('admin::app.catalog.products.related-products') }}
            </label>
            <label for="similar_colors" v-if="(key == 'similar_colors')">
                Продукты, схожие по цвету
            </label>

{{--            <input type="text" class="control" autocomplete="off" v-model="search_term[key]" placeholder="{{ __('admin::app.catalog.products.product-search-hint') }}" v-on:keyup="search(key)">--}}

            <div class="btn btn-primary btn-lg mb-10 mt-20" @click="showAddedModalForm(key)">Добавить продукты</div>

            <div>
                <datagrid-plus v-if="(key == 'up_sells')" :key="updateKey[key]" type="linkedProducts" src="{{ route('admin.catalog.products.linkedGrid', [$product->id, 'up_sells']) }}" :productsType="key" :updateGrid="updateGrid" productId="{{ $product->id }}">
                </datagrid-plus>
                <datagrid-plus v-if="(key == 'cross_sells')" :key="updateKey[key]" type="linkedProducts" src="{{ route('admin.catalog.products.linkedGrid', [$product->id, 'cross_sells']) }}" :productsType="key" :updateGrid="updateGrid" productId="{{ $product->id }}">
                </datagrid-plus>
                <datagrid-plus v-if="(key == 'related_products')" :key="updateKey[key]" type="linkedProducts" src="{{ route('admin.catalog.products.linkedGrid', [$product->id, 'related_products']) }}" :productsType="key" :updateGrid="updateGrid" productId="{{ $product->id }}">
                </datagrid-plus>
                <datagrid-plus v-if="(key == 'similar_colors')" :key="updateKey[key]" type="linkedProducts" src="{{ route('admin.catalog.products.linkedGrid', [$product->id, 'similar_colors']) }}" :productsType="key" :updateGrid="updateGrid" productId="{{ $product->id }}">
                </datagrid-plus>
            </div>

            <div class="linked-product-search-result">
                <ul>
                    <li v-for='(product, index) in products[key]' v-if='products[key].length' @click="addProduct(product, key)">
                        @{{ product.name }}
                    </li>

                    <li v-if='! products[key].length && search_term[key].length && ! is_searching[key]'>
                        {{ __('admin::app.catalog.products.no-result-found') }}
                    </li>

                    <li v-if="is_searching[key] && search_term[key].length">
                        {{ __('admin::app.catalog.products.searching') }}
                    </li>
                </ul>
            </div>

            <input type="hidden" name="up_sell[]" v-for='(product, index) in addedProducts.up_sells' v-if="(key == 'up_sells') && addedProducts.up_sells.length" :value="product.id"/>

            <input type="hidden" name="cross_sell[]" v-for='(product, index) in addedProducts.cross_sells' v-if="(key == 'cross_sells') && addedProducts.cross_sells.length" :value="product.id"/>

            <input type="hidden" name="related_products[]" v-for='(product, index) in addedProducts.related_products' v-if="(key == 'related_products') && addedProducts.related_products.length" :value="product.id"/>

{{--            <span class="filter-tag linked-product-filter-tag" v-if="addedProducts[key].length">--}}
{{--                <span class="wrapper linked-product-wrapper " v-for='(product, index) in addedProducts[key]'>--}}
{{--                    <span class="do-not-cross-linked-product-arrow">--}}
{{--                        @{{ product.name }}--}}
{{--                    </span>--}}
{{--                    <span class="icon cross-icon" @click="removeProduct(product, key)"></span>--}}
{{--                </span>--}}
{{--            </span>--}}
        </div>

    </div>
</script>

<script>

    Vue.component('linked-products', {

        template: '#linked-products-template',

        data: function() {
            return {
                products: {
                    'cross_sells': [],
                    'similar_colors': [],
                    'up_sells': [],
                    'related_products': []
                },

                updateKey: {
                    'cross_sells': 0,
                    'similar_colors': 0,
                    'up_sells': 0,
                    'related_products': 0
                },

                productsType: '',

                search_term: {
                    'cross_sells': '',
                    'similar_colors': '',
                    'up_sells': '',
                    'related_products': ''
                },

                addedProducts: {
                    'cross_sells': [],
                    'similar_colors': [],
                    'up_sells': [],
                    'related_products': []
                },

                is_searching: {
                    'cross_sells': false,
                    'similar_colors': false,
                    'up_sells': false,
                    'related_products': false
                },

                productId: {{ $product->id }},

                linkedProducts: ['up_sells', 'cross_sells', 'related_products', 'similar_colors'],

                upSellingProducts: @json($product->up_sells()->get()),

                crossSellingProducts: @json($product->cross_sells()->get()),

                relatedProducts: @json($product->related_products()->get()),
            }
        },

        created: function () {
            if (this.upSellingProducts.length >= 1) {
                for (var index in this.upSellingProducts) {
                    this.addedProducts.up_sells.push(this.upSellingProducts[index]);
                }
            }

            if (this.crossSellingProducts.length >= 1) {
                for (var index in this.crossSellingProducts) {
                    this.addedProducts.cross_sells.push(this.crossSellingProducts[index]);
                }
            }

            if (this.relatedProducts.length >= 1) {
                for (var index in this.relatedProducts) {
                    this.addedProducts.related_products.push(this.relatedProducts[index]);
                }
            }
        },

        methods: {
            updateGrid(key) {
                this.$root.$set(this.$root.modalIds, 'productsTable', false);
                this.updateKey[key]++;
            },

            showAddedModalForm(key) {
                this.productsType = key;
                this.$root.showModal('productsTable');
            },

            addProduct: function (product, key) {
                this.addedProducts[key].push(product);
                this.search_term[key] = '';
                this.products[key] = [];
                this.updateKey[key]++;
            },

            removeProduct: function (product, key) {
                for (var index in this.addedProducts[key]) {
                    if (this.addedProducts[key][index].id == product.id ) {
                        this.addedProducts[key].splice(index, 1);
                    }
                }
            },

            search: function (key) {
                this_this = this;

                this.is_searching[key] = true;

                if (this.search_term[key].length >= 1) {
                    this.$http.get ("{{ route('admin.catalog.products.productlinksearch') }}", {params: {query: this.search_term[key]}})
                        .then (function(response) {

                            for (var index in response.data) {
                                if (response.data[index].id == this_this.productId) {
                                    response.data.splice(index, 1);
                                }
                            }

                            if (this_this.addedProducts[key].length) {
                                for (var product in this_this.addedProducts[key]) {
                                    for (var productId in response.data) {
                                        if (response.data[productId].id == this_this.addedProducts[key][product].id) {
                                            response.data.splice(productId, 1);
                                        }
                                    }
                                }
                            }

                            this_this.products[key] = response.data;

                            this_this.is_searching[key] = false;
                        })

                        .catch (function (error) {
                            this_this.is_searching[key] = false;
                        })
                } else {
                    this_this.products[key] = [];
                    this_this.is_searching[key] = false;
                }
            }
        }
    });

</script>

@endpush