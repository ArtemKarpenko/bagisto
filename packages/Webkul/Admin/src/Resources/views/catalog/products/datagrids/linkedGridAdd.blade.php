@extends('admin::layouts.content')

@section('page_title')
    Продукты
@stop

@section('content')
    <div class="content">
        <div class="page-header">

        </div>

        <div class="page-content">

            {!! app('Webkul\Admin\DataGrids\ProductLinkedAddDataGrid')->render() !!}

        </div>
    </div>
@stop
