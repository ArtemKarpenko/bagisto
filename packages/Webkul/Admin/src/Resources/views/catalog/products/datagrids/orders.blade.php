@extends('admin::layouts.content')

@section('page_title')
    Заголовок
@stop

@section('content')
    <div class="content">
        <div class="page-header">

        </div>

        <div class="page-content">

            {!! app('Webkul\Admin\DataGrids\CustomerOrdersDataGrid')->render() !!}

        </div>
    </div>
@stop
