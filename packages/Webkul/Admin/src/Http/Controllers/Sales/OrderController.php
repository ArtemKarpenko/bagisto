<?php

namespace Webkul\Admin\Http\Controllers\Sales;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Webkul\Admin\DataGrids\OrderDataGrid;
use Webkul\Admin\Http\Controllers\Controller;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Sales\Repositories\OrderItemRepository;
use Webkul\Sales\Repositories\OrderRepository;
use \Webkul\Sales\Repositories\OrderCommentRepository;
use Webkul\Sales\Repositories\RefundRepository;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_config;

    /**
     * OrderRepository object
     *
     * @var \Webkul\Sales\Repositories\OrderRepository
     */
    protected $orderRepository;

    protected $orderItemRepository;

    protected $refundRepository;


    /**
     * OrderCommentRepository object
     *
     * @var \Webkul\Sales\Repositories\OrderCommentRepository
     */
    protected $orderCommentRepository;

    protected $productRepository;

    /**
     * Create a new controller instance.
     *
     * @param  \Webkul\Sales\Repositories\OrderRepository  $orderRepository
     * @param  \Webkul\Sales\Repositories\OrderCommentRepository  $orderCommentRepository
     * @return void
     */
    public function __construct(
        OrderRepository $orderRepository,
        OrderCommentRepository $orderCommentRepository,
        OrderItemRepository $orderItemRepository,
        RefundRepository $refundRepository,
        ProductRepository $productRepository
    )
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->orderRepository = $orderRepository;

        $this->orderCommentRepository = $orderCommentRepository;

        $this->orderItemRepository = $orderItemRepository;

        $this->refundRepository = $refundRepository;

        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        if (request()->ajax()) {
            return app(OrderDataGrid::class)->toJson();
        }

        return view($this->_config['view']);
    }

    public function deleteItem($orderId)
    {
        $data = request()->all();
        $order = $this->orderRepository->findOrFail($orderId);
        $orderItem = $this->orderItemRepository->find($data['itemId']);
        $orderItem->delete();
        $orderItems = $order->items;

        $orderItemsCount = [];
        $totalQuantity = 0;

        foreach ($orderItems as $item) {
            $orderItemsCount[$item->id] = $item->qty_ordered;
            $totalQuantity += $item->qty_ordered;
        }

        if ($totalQuantity == 0){
            $order->delete();
            return ['url' => route($this->_config['redirect'])];
        }

        $summary = $this->orderRepository->getOrderSummary($orderItemsCount, $order);
        $this->orderRepository->updateSummaryOrder($order, $summary, $totalQuantity);

        return ['url' => route('admin.sales.orders.view', $order->id)];
    }

    public function updateQuantity($productId)
    {
        $data = request()->all();

        $order = $this->orderRepository->findOrFail($data['order_id']);
        $item = $order->items->find($productId);
        $orderItem = $this->orderItemRepository->find($item->id);
        $qty = (int)$data['quantity'];
        $this->orderItemRepository->updateQuantity($orderItem, $qty);

        $orderItems = $order->items;

        $orderItemsCount = [];
        $totalQuantity = 0;

        foreach ($orderItems as $item) {
            if ($item->id == $orderItem->id){
                $orderItemsCount[$item->id] = $qty;
                $totalQuantity += $qty;
            }else{
                $orderItemsCount[$item->id] = $item->qty_ordered;
                $totalQuantity += $item->qty_ordered;
            }
        }

        $summary = $this->orderRepository->getOrderSummary($orderItemsCount, $order);
        $this->orderRepository->updateSummaryOrder($order, $summary, $totalQuantity);
        return json_encode(['status' => true, 'updatedTotal' => $data['quantity']]);
    }

    public function address($id)
    {
        try {
            $order = $this->orderRepository->findOrFail($id);
            $this->validate(request(), [
                'address1'  => 'required',
                'postcode'  => 'required',
                'city'  => 'required',
                'state'  => 'required',
                'phone' => 'required',
                'country'=> 'required',
            ]);

            $data = request()->all();
            $order->shipping_address->update($data);
            return response()->json(['message' => 'Адрес доставки успешно обновлен'], 200);
        } catch (Exception $e) {
            report($e);

            session()->flash('error', 'Возникла ошибка при обновлении адреса доставки');
        }
        return response()->json(['message' => 'Возникла ошибка при обновлении адреса доставки'], 400);
    }

    public function addItems()
    {
        $parameters = Route::current()->parameters();
        $id = $parameters['orderId'];
        $order = $this->orderRepository->findOrFail($id);
        if (!$order->isPending()){
            session()->flash('error', 'Для добавления продуктов статус заказа должен быть - В ожидании');
            return redirect()->route($this->_config['redirect'], $order->id);
        }
        $data = request()->all();
        $productIds = explode(',', $data['indexes']);
        $items = $order->items->pluck('product_id')->toArray();
        $addedCount = 0;

        foreach ($productIds as $productId) {
            $data = ['quantity' => 1, 'product_id' => $productId, "_token" => $data['_token']];
            $product = $this->productRepository->findOneByField('id', $productId);

            if ($product->status === 0) {
                continue;
            }

            if (in_array((int)$productId, $items)){
                $orderItem = $this->orderItemRepository->findWhere(['order_id' => $order->id, 'product_id' =>  $productId])->first();
                $qty = $orderItem->qty_ordered + 1;

                $this->orderItemRepository->updateQuantity($orderItem, $qty);

                $addedCount++;
            }else{
                $orderProduct = $product->getTypeInstance()->prepareForOrder($data);
                if (!is_array($orderProduct)) {
                    continue;
                }
                $orderProduct['qty_ordered'] = 1;
                $orderProduct['product_type'] = 'ACME\HelloWorld\Models\Product';

                $orderItem = $this->orderRepository->createOrderItem($orderProduct, $order, $order);
                $addedCount++;
            }
        }

        if ($addedCount > 0) {
            count($productIds) > 1 ? session()->flash('success', 'Продукты успешно добавлены к заказу') : session()->flash('success', 'Продукт успешно добавлен к заказу');
        }else{
            session()->flash('error', 'Продукты не добавлены к заказу');
            return redirect()->route($this->_config['redirect'], $order->id);
        }

        $order = $this->orderRepository->findOrFail($id);

        $orderItems = $order->items;

        $orderItemsCount = [];
        $totalQuantity = 0;

        foreach ($orderItems as $item) {
            $orderItemsCount[$item->id] = $item->qty_ordered;
            $totalQuantity += $item->qty_ordered;
        }

        $summary = $this->orderRepository->getOrderSummary($orderItemsCount, $order);
        $this->orderRepository->updateSummaryOrder($order, $summary, $totalQuantity);

        return redirect()->route($this->_config['redirect'], $order->id);
    }

    public function addItem($id)
    {
        $request_data = request()->all();
        $productId = $request_data['product_id'];
        $data = ['quantity' => 1, 'product_id' => $productId, "_token" => $request_data['token']];
        $product = $this->productRepository->findOneByField('id', $productId);

        if ($product->status === 0) {
            return ['info' => __('shop::app.checkout.cart.item.inactive-add')];
        }

        $orderProduct = $product->getTypeInstance()->prepareForOrder($data);
        if (!is_array($orderProduct)) {
            session()->flash('error', $orderProduct);
            return ['info' => $orderProduct];
        }
        $orderProduct['qty_ordered'] = 1;
        $orderProduct['product_type'] = 'ACME\HelloWorld\Models\Product';

        $order = $this->orderRepository->findOrFail($id);

        $orderItem = $this->orderRepository->createOrderItem($orderProduct, $order, $order);

        $order = $this->orderRepository->findOrFail($id);

        $orderItems = $order->items;

        $orderItemsCount = [];
        $totalQuantity = 0;

        foreach ($orderItems as $item) {
            $orderItemsCount[$item->id] = $item->qty_ordered;
            $totalQuantity += $item->qty_ordered;
        }

        $summary = $this->orderRepository->getOrderSummary($orderItemsCount, $order);
        $this->orderRepository->updateSummaryOrder($order, $summary, $totalQuantity);

        session()->flash('success', 'Продукт успешно добавлен к заказу');
    }

    /**
     * Show the view for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function view($id)
    {
        $order = $this->orderRepository->findOrFail($id);

        return view($this->_config['view'], compact('order'));
    }

    /**
     * Cancel action for the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel($id)
    {
        $result = $this->orderRepository->cancel($id);

        if ($result) {
            session()->flash('success', trans('admin::app.response.cancel-success', ['name' => 'Order']));
        } else {
            session()->flash('error', trans('admin::app.response.cancel-error', ['name' => 'Order']));
        }

        return redirect()->back();
    }

    /**
     * Add comment to the order
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function comment($id)
    {
        $data = array_merge(request()->all(), [
            'order_id' => $id,
        ]);

        $data['customer_notified'] = isset($data['customer_notified']) ? 1 : 0;

        Event::dispatch('sales.order.comment.create.before', $data);

        $comment = $this->orderCommentRepository->create($data);

        Event::dispatch('sales.order.comment.create.after', $comment);

        session()->flash('success', trans('admin::app.sales.orders.comment-added-success'));

        return redirect()->back();
    }
}