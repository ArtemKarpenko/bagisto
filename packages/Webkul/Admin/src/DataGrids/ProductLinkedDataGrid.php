<?php

namespace Webkul\Admin\DataGrids;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Webkul\Core\Models\Channel;
use Webkul\Core\Models\Locale;
use Webkul\Inventory\Repositories\InventorySourceRepository;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Ui\DataGrid\DataGrid;
use Webkul\Product\Facades\ProductImage as ProductImageFacade;
use Webkul\Ui\DataGrid\Traits\ProvideDataGridPlus;
use Webkul\Admin\DataGrids\ProductDataGrid;

class ProductLinkedDataGrid extends ProductDataGrid
{
    use ProvideDataGridPlus;


    /**
     * Default sort order of datagrid.
     *
     * @var string
     */
    protected $sortOrder = 'asc';

    /**
     * Set index columns, ex: id.
     *
     * @var string
     */
    protected $index = 'product_id';

    /**
     * Set default sorting column
     *
     * @var string
     */
    protected $defaultSortColumn = 'sort';

//    protected $itemsPerPage = 25;

    /**
     * Prepare query builder.
     *
     * @return void
     */
    public function prepareQueryBuilder()
    {
        if ($this->channel === 'all') {
            $whereInChannels = Channel::query()->pluck('code')->toArray();
        } else {
            $whereInChannels = [$this->channel];
        }

        if ($this->locale === 'all') {
            $whereInLocales = Locale::query()->pluck('code')->toArray();
        } else {
            $whereInLocales = [$this->locale];
        }

        $parameters = Route::current()->parameters();
        $productId = $parameters['productId'];
        $key = $parameters['key'];
        $columnLink = 'linked_table.child_id';
        $parentColumn = 'linked_table.parent_id';

        switch ($key) {
            case 'up_sells':
                $tableLeft = 'product_up_sells as linked_table';
                break;
            case 'cross_sells':
                $tableLeft = 'product_cross_sells as linked_table';
                break;
            case 'related_products':
                $tableLeft = 'product_relations as linked_table';
                break;
            case 'similar_colors':
                $tableLeft = 'product_similar_colors as linked_table';
                break;
        }


        /* query builder */
        $queryBuilder = DB::table('product_flat')
            ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
            ->leftJoin('attribute_families', 'products.attribute_family_id', '=', 'attribute_families.id')
            ->leftJoin('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
            ->leftJoin($tableLeft, $columnLink, '=', 'products.id')
            ->select(
                'product_flat.locale',
                'product_flat.channel',
                'product_flat.product_id',
                'products.sku as product_sku',
                'product_flat.product_number',
                'product_flat.name as product_name',
                'products.type as product_type',
                'product_flat.status',
                'product_flat.price',
                'attribute_families.name as attribute_family',
                DB::raw('SUM(' . DB::getTablePrefix() . 'product_inventories.qty) as quantity'),
                'linked_table.sort'
            );

        $queryBuilder->groupBy('product_flat.product_id', 'product_flat.locale', 'product_flat.channel');

        $queryBuilder->whereIn('product_flat.locale', $whereInLocales);
        $queryBuilder->whereIn('product_flat.channel', $whereInChannels);
        $queryBuilder->where($parentColumn, $productId);
//        $queryBuilder->orderBy('linked_table.sort', 'ASC');

        $this->addFilter('product_id', 'product_flat.product_id');
        $this->addFilter('product_name', 'product_flat.name');
        $this->addFilter('product_sku', 'products.sku');
        $this->addFilter('product_number', 'product_flat.product_number');
        $this->addFilter('status', 'product_flat.status');
        $this->addFilter('product_type', 'products.type');
        $this->addFilter('attribute_family', 'attribute_families.name');

        $this->setQueryBuilder($queryBuilder);
    }


    /**
     * Add columns.
     *
     * @return void
     */
    public function addColumns()
    {
        parent::addColumns();

        $this->addColumn([
            'index'      => 'sort',
            'label'      => 'sort',
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => false,
        ]);
    }



    /**
     * Prepare actions.
     *
     * @return void
     */
    public function prepareActions()
    {
        $this->addAction([
            'title'     => trans('admin::app.datagrid.edit'),
            'method'    => 'GET',
            'route'     => 'admin.catalog.products.edit',
            'icon'      => 'icon pencil-lg-icon',
            'condition' => function () {
                return true;
            },
        ]);

//        $this->addAction([
//            'title'     => 'Переместить',
//            'method'    => '',
//            'route'     => 'admin.catalog.products.edit',
//            'icon'      => 'icon list-icon handle',
//            'function'     => 'testFunc()'
////            'condition' => function () {
////                return true;
////            },
//        ]);
    }

    /**
     * Prepare mass actions.
     *
     * @return void
     */
    public function prepareMassActions()
    {
        $parameters = Route::current()->parameters();
        $productId = $parameters['productId'];
        $key = $parameters['key'];
        $this->addMassAction([
            'type'    => 'delete',
            'label'   => 'Удалить',
            'action'  => route('admin.catalog.products.remove-linked-product', [$productId, $key]),
            'method'  => 'POST',
        ]);
    }


}
