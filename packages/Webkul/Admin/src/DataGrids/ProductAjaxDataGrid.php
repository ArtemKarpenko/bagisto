<?php

namespace Webkul\Admin\DataGrids;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Webkul\Core\Models\Channel;
use Webkul\Core\Models\Locale;
use Webkul\Inventory\Repositories\InventorySourceRepository;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Ui\DataGrid\DataGrid;
use Webkul\Product\Facades\ProductImage as ProductImageFacade;
use Webkul\Ui\DataGrid\Traits\ProvideDataGridPlus;
use Webkul\Admin\DataGrids\ProductDataGrid;

class ProductAjaxDataGrid extends ProductDataGrid
{
    use ProvideDataGridPlus;

    /**
     * Prepare actions.
     *
     * @return void
     */
    public function prepareActions()
    {
        $this->addAction([
            'title'     => trans('admin::app.datagrid.edit'),
            'method'    => 'GET',
            'route'     => 'admin.catalog.products.edit',
            'icon'      => 'icon pencil-lg-icon',
            'condition' => function () {
                return true;
            },
        ]);
    }

    /**
     * Prepare mass actions.
     *
     * @return void
     */
    public function prepareMassActions()
    {
        $parameters = Route::current()->parameters();
        $orderId = $parameters['orderId'];
        $this->addMassAction([
            'type'    => 'delete',
            'label'   => 'Добавить к заказу',
            'action'  => route('admin.sales.orders.add-items', $orderId),
            'method'  => 'POST',
        ]);
    }


}
