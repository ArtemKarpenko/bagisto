<?php

namespace Webkul\Admin\DataGrids;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Webkul\Ui\DataGrid\DataGrid;
use Webkul\Ui\DataGrid\Traits\ProvideDataGridPlus;

class CustomerOrdersDataGrid extends DataGrid
{
    use ProvideDataGridPlus;

    protected $index = 'id';

    protected $sortOrder = 'desc';

    protected $customSettings = [];

    public function prepareQueryBuilder()
    {
        $id = request('id');

        $queryBuilder = DB::table('orders as o')
            ->leftJoin('order_items as oi', 'oi.order_id', '=', 'o.id')
            ->groupBy('o.id')
            ->addSelect('o.id', DB::raw('CONCAT(o.customer_first_name, " ", o.customer_last_name) AS full_name'), 'o.base_grand_total', 'o.status', 'o.customer_id')
            ->where('oi.product_id', $id);

//        $queryBuilder = DB::table('customers as c')
//            ->leftJoin('orders as o', 'o.customer_id', '=', 'c.id')
//            ->leftJoin('order_items as oi', 'oi.order_id', '=', 'o.id')
//            ->groupBy('o.id')
//            ->addSelect('c.id', 'o.id as order_id', DB::raw('CONCAT(c.first_name, " ", c.last_name) AS full_name'), 'o.base_grand_total', 'o.status')
//            ->where('oi.product_id', $id);

        $this->setQueryBuilder($queryBuilder);
    }

    /**
     * Подготавливаем массив с кастомными настройками.
     *
     * @return void
     */
    public function prepareCustomSettings()
    {
        $this->customSettings = [
            'isShowFilter' => true,
            'isDraggableRows' => true,
            'draggableAction' => null,
            'draggableIdName' => 'id',
        ];
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => 'Id заказа',
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'full_name',
            'label'      => 'Имя клиента',
            'type'       => 'string',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => false,
            'closure' => function ($value) {
                if (!empty($value->customer_id)) {
                    return '<a target="_blank" href="' . route('admin.customer.edit', $value->customer_id) . '">' . $value->full_name . '</a>';
                }else{
                    return $value->full_name;
                }
            },
        ]);

        $this->addColumn([
            'index'      => 'base_grand_total',
            'label'      => 'Сумма заказа',
            'type'       => 'number',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'status',
            'label'      => trans('admin::app.datagrid.status'),
            'type'       => 'string',
            'sortable'   => true,
            'searchable' => true,
            'filterable' => true,
            'closure' => function ($value) {
                if ($value->status == 'processing') {
                    return '<span class="badge badge-md badge-success">' . trans('admin::app.sales.orders.order-status-processing') . '</span>';
                } elseif ($value->status == 'completed') {
                    return '<span class="badge badge-md badge-success">' . trans('admin::app.sales.orders.order-status-success') . '</span>';
                } elseif ($value->status == "canceled") {
                    return '<span class="badge badge-md badge-danger">' . trans('admin::app.sales.orders.order-status-canceled') . '</span>';
                } elseif ($value->status == "closed") {
                    return '<span class="badge badge-md badge-info">' . trans('admin::app.sales.orders.order-status-closed') . '</span>';
                } elseif ($value->status == "pending") {
                    return '<span class="badge badge-md badge-warning">' . trans('admin::app.sales.orders.order-status-pending') . '</span>';
                } elseif ($value->status == "pending_payment") {
                    return '<span class="badge badge-md badge-warning">' . trans('admin::app.sales.orders.order-status-pending-payment') . '</span>';
                } elseif ($value->status == "fraud") {
                    return '<span class="badge badge-md badge-danger">' . trans('admin::app.sales.orders.order-status-fraud') . '</span>';
                }
            },
        ]);
    }

    public function prepareActions()
    {
        $this->addAction([
            'title'  => trans('admin::app.datagrid.view'),
            'method' => 'GET',
            'route'  => 'admin.sales.orders.view',
            'icon'   => 'icon eye-icon',
        ]);
    }
}