<?php

namespace Webkul\Category\DataGrids;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Webkul\Core\Models\Channel;
use Webkul\Core\Models\Locale;
use Webkul\Inventory\Repositories\InventorySourceRepository;
use Webkul\Product\Facades\ProductImage as ProductImageFacade;
use Webkul\Product\Repositories\ProductRepository;
use Webkul\Ui\DataGrid\DataGrid;
use Webkul\Ui\DataGrid\Traits\ProvideDataGridPlus;
use Webkul\Admin\DataGrids\ProductDataGrid;
use Webkul\LookBook\Models\LookBookPage;

class CategoryProductsDataGrid extends ProductDataGrid
{
    use ProvideDataGridPlus;

    protected $index = 'product_id';

    protected $sortOrder = 'desc';

    protected $gridType = 'view';

    protected $categoryId = null;

    protected $customSettings = [];

    /**
     * Set default sorting column
     *
     * @var string
     */
    protected $defaultSortColumn = null;

    /**
     * Prepare query builder.
     *
     * @return void
     */
    public function prepareQueryBuilder()
    {
        if ($this->channel === 'all') {
            $whereInChannels = Channel::query()->pluck('code')->toArray();
        } else {
            $whereInChannels = [$this->channel];
        }

        if ($this->locale === 'all') {
            $whereInLocales = Locale::query()->pluck('code')->toArray();
        } else {
            $whereInLocales = [$this->locale];
        }

        $parameters = Route::current()->parameters();
        if(array_key_exists('type', $parameters) && $parameters['type'] === 'add'){
            $this->gridType = 'add';
        }
        $this->categoryId = $parameters['categoryId'];

        /* query builder */
        $queryBuilder = DB::table('product_flat')
            ->leftJoin('products', 'product_flat.product_id', '=', 'products.id')
            ->leftJoin('attribute_families', 'products.attribute_family_id', '=', 'attribute_families.id')
            ->leftJoin('product_inventories', 'product_flat.product_id', '=', 'product_inventories.product_id')
            ->leftJoin('colors', 'products.color_id', '=', 'colors.id')
            ->select(
                'product_flat.locale',
                'product_flat.channel',
                'product_flat.product_id',
                'products.id',
                'products.sku as product_sku',
                'product_flat.product_number',
                'product_flat.name as product_name',
                'products.type as product_type',
                'product_flat.status',
                'product_flat.price',
                'colors.hex_code',
                'attribute_families.name as attribute_family',
                DB::raw('SUM(' . DB::getTablePrefix() . 'product_inventories.qty) as quantity')
            );

        if ($this->gridType === 'view'){
            $queryBuilder->addSelect('product_categories.sort');
            $queryBuilder->leftJoin('product_categories', 'product_categories.product_id', '=', 'products.id');
            $queryBuilder->where('product_categories.category_id', $this->categoryId);
            $this->defaultSortColumn = 'sort';
            $this->sortOrder = 'asc';
        }else{
            $categoryProducts = DB::table('product_categories')
                ->select('product_id')
                ->where('product_categories.category_id', $this->categoryId)
                ->get()
                ->pluck('product_id')
                ->toArray();
            $queryBuilder->whereNotIn('products.id', $categoryProducts);
        }

        $queryBuilder->groupBy('product_flat.product_id', 'product_flat.locale', 'product_flat.channel');

//        $queryBuilder->whereIn('product_flat.locale', $whereInLocales);
//        $queryBuilder->whereIn('product_flat.channel', $whereInChannels);
        $queryBuilder->where('products.parent_id', NULL);

        $this->addFilter('product_id', 'product_flat.product_id');
        $this->addFilter('product_name', 'product_flat.name');
        $this->addFilter('product_sku', 'products.sku');
        $this->addFilter('product_number', 'product_flat.product_number');
        $this->addFilter('status', 'product_flat.status');
        $this->addFilter('product_type', 'products.type');
        $this->addFilter('attribute_family', 'attribute_families.name');

        $this->setQueryBuilder($queryBuilder);
    }

    /**
     * Подготавливаем массив с кастомными настройками.
     *
     * @return void
     */
    public function prepareCustomSettings()
    {
        $parameters = Route::current()->parameters();
        if(array_key_exists('type', $parameters) && $parameters['type'] === 'add'){
            $this->customSettings = [
                'isShowFilter' => true,
                'isDraggableRows' => false,
            ];
        }else{
            $this->customSettings = [
                'isShowFilter' => false,
                'isDraggableRows' => true,
                'draggableAction' => route('admin.catalog.categories.products.drag', $parameters['categoryId']),
                'draggableIdName' => 'id',
            ];
        }
    }

    /**
     * Prepare button mass actions.
     *
     * @return void
     */
    public function prepareButtonMassActions()
    {
        $parameters = Route::current()->parameters();
        if(array_key_exists('type', $parameters) && $parameters['type'] === 'add'){
            $this->gridType = 'add';
        }
        $this->categoryId = $parameters['categoryId'];
        if ($this->gridType === 'add') {
            $this->addButtonMassAction([
                'button' => [
                    'label' => 'Добавить',
                    'class' => 'btn-primary btn-lg',
                ],
                'modal' => [
                    'text' => 'Добавить выбранные продукты к категории?',
                    'action' => route('admin.catalog.categories.products.add', $this->categoryId),
                    'is_second' => true,
                ],
            ]);
        }
        if ($this->gridType === 'view') {
            $this->addButtonMassAction([
                'button' => [
                    'label' => 'Удалить',
                    'class' => 'btn-danger btn-lg',
                ],
                'modal' => [
                    'text' => 'Удалить выбранные продукты из категории?',
                    'action' => route('admin.catalog.categories.products.delete', $this->categoryId),
                    'is_second' => false,
                ],
            ]);
        }
    }

    /**
     * Prepare mass actions.
     *
     * @return void
     */
    public function prepareMassActions()
    {
    }

    /**
     * Add columns.
     *
     * @return void
     */
    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'product_id',
            'label'      => trans('admin::app.datagrid.id'),
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
//            'closure'    => function ($row) {
//                if (!empty($row->variants) && count($row->variants)){
//                    return '<div class="toggle_childs arrow-down-icon">' . $row->id . '</div>';
//                }else{
//                    return $row->id;
//                }
//            },
        ]);

        $this->addColumn([
            'index'      => 'image',
            'label'      => 'Фото',
            'type'       => 'string',
            'sortable'   => false,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                return $this->renderImageView($row);
            },
        ]);

        $this->addColumn([
            'index'      => 'product_name',
            'label'      => trans('admin::app.datagrid.name'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'product_sku',
            'label'      => trans('admin::app.datagrid.sku'),
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => '',
            'label'      => 'Цвет фильтра',
            'type'       => 'string',
            'sortable'   => false,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                if(isset($row->hex_code)){
                    return '<div style="border-radius: 5px; width: 20px; height: 20px; background-color: ' . $row->hex_code . ';"></div>';
                }else{

                }

            },
        ]);

        $this->addColumn([
            'index'      => 'status',
            'label'      => trans('admin::app.datagrid.status'),
            'type'       => 'boolean',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => true,
            'closure'    => function ($value) {
                if ($value->status == 1) {
                    return trans('admin::app.datagrid.active');
                } else {
                    return trans('admin::app.datagrid.inactive');
                }
            },
        ]);

        $this->addColumn([
            'index'      => 'quantity',
            'label'      => trans('admin::app.datagrid.qty'),
            'type'       => 'number',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => false,
            'closure'    => function ($row) {
                if (is_null($row->quantity)) {
                    return 0;
                } else {
                    return $this->renderQuantityView($row);
                }
            },
        ]);

//        $this->addColumn([
//            'index'      => 'product_number',
//            'label'      => trans('admin::app.datagrid.product-number'),
//            'type'       => 'string',
//            'searchable' => true,
//            'sortable'   => true,
//            'filterable' => true,
//        ]);

//        $this->addColumn([
//            'index'      => 'attribute_family',
//            'label'      => trans('admin::app.datagrid.attribute-family'),
//            'type'       => 'string',
//            'searchable' => true,
//            'sortable'   => true,
//            'filterable' => true,
//        ]);

//        $this->addColumn([
//            'index'      => 'product_type',
//            'label'      => trans('admin::app.datagrid.type'),
//            'type'       => 'string',
//            'sortable'   => true,
//            'searchable' => true,
//            'filterable' => true,
//        ]);

        $this->addColumn([
            'index'      => 'price',
            'label'      => trans('admin::app.datagrid.price'),
            'type'       => 'price',
            'sortable'   => true,
            'searchable' => false,
            'filterable' => true,
            'closure'    => function ($row) {
                if ($row->product_type == 'configurable'){
                    return $this->renderMinMaxPrice($row);
                }else{
                    return $row->price;
                }

            },
        ]);

        $parameters = Route::current()->parameters();
        if(array_key_exists('type', $parameters) && $parameters['type'] === 'add'){
//            $this->gridType = 'add';
        }else{
            $this->addColumn([
                'index'      => 'sort',
                'label'      => 'sort',
                'type'       => 'number',
                'searchable' => false,
                'sortable'   => true,
                'filterable' => false,
            ]);
        }
    }
    /**
     * Prepare actions.
     *
     * @return void
     */
    public function prepareActions()
    {
        $this->addAction([
            'title'     => trans('admin::app.datagrid.edit'),
            'method'    => 'GET',
            'route'     => 'admin.catalog.products.edit',
            'icon'      => 'icon pencil-lg-icon',
            'condition' => function () {
                return true;
            },
        ]);
    }

    /**
     * Render quantity view.
     *
     * @parma  object  $row
     * @return \Illuminate\Contracts\View\View|\Illuminate\Contracts\View\Factory
     */
    private function renderQuantityView($row)
    {
        $product = $this->productRepository->find($row->id);

        $inventorySources = $this->inventorySourceRepository->findWhere(['status' => 1]);

        $totalQuantity = $row->quantity;

        return view('admin::catalog.products.datagrid.quantity', compact('product', 'inventorySources', 'totalQuantity'))->render();
    }

    private function renderMinMaxPrice($row)
    {
        $product = $this->productRepository->find($row->id);

        $prices = $product->getTypeInstance()->getMinMaxPrice();

        return $prices['min'] . ' - ' . $prices['max'];
    }

    private function renderImageView($row)
    {
        $product = $this->productRepository->find($row->id);

        $images = ProductImageFacade::getProductBaseImage($product);

        $image = count($images) ? $images['small_image_url'] : null;

        $id = $row->id;

        return view('admin::catalog.products.datagrid.image', compact('image', 'id'))->render();
    }
}