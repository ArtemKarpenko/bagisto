@extends('admin::layouts.content')

@section('page_title')
    Акции и баннеры
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Акции и баннеры</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.promotion.promotion.create') }}" class="btn btn-lg btn-primary">
                    Добавить акцию
                </a>
                <a href="{{ route('admin.promotion.banner.create') }}" class="btn btn-lg btn-primary">
                    Добавить баннер
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('promotionGroup','Webkul\Promotion\DataGrids\PromotionDataGrid')
            {!! $promotionGroup->render() !!}
        </div>
    </div>

@stop