@extends('admin::layouts.content')

@section('page_title')
    Добавить акцию
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.promotion.store') }}" @submit.prevent="onSubmit">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="window.location = '{{ route('admin.promotion.index') }}'"></i>

                        Добавить акцию
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Сохранить акцию
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <input type="hidden" name="type" value="promotion">

                    <div class="control-group" :class="[errors.has('title') ? 'has-error' : '']">
                        <label for="title" class="required">
                            Наименование
                        </label>
                        <input type="text" class="control" name="title" v-validate="'required'" value="{{ old('title') }}" data-vv-as="&quot;Наименование&quot;">
                        <span class="control-error" v-if="errors.has('title')">@{{ errors.first('title') }}</span>
                    </div>

                    <div class="control-group">
                        <label for="status">Статус</label>

                        <label class="switch">
                            <input type="checkbox" id="status" name="status" value="1">
                            <span class="slider round"></span>
                        </label>
                    </div>

                    <div class="control-group" :class="[errors.has('description') ? 'has-error' : '']">
                        <label for="description">
                            Описание для клиента
                        </label>
                        <textarea name="description" data-vv-as="&quot;Описание&quot;" class="control">{{ old('description') }}</textarea>
                        <span class="control-error" v-if="errors.has('description')">@{{ errors.first('description') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('seller_description') ? 'has-error' : '']">
                        <label for="seller_description">
                            Описание для продавца
                        </label>
                        <textarea name="seller_description" data-vv-as="&quot;Описание для продавца&quot;" class="control">{{ old('seller_description') }}</textarea>
                        <span class="control-error" v-if="errors.has('seller_description')">@{{ errors.first('seller_description') }}</span>
                    </div>

                </div>
            </div>

        </form>
    </div>
@stop