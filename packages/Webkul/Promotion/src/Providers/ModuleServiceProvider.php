<?php

namespace Webkul\Promotion\Providers;

use Konekt\Concord\BaseModuleServiceProvider;

class ModuleServiceProvider extends BaseModuleServiceProvider
{
    protected $models = [
        \Webkul\Promotion\Models\Promotion::class,
    ];
}