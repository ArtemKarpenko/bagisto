<?php

return [
    [
        'key' => 'promotion',
        'name' => 'Promotion',
        'route' => 'admin.promotion.index',
        'sort' => 2
    ]
];