<?php

return [
    [
    'key' => 'promotion',
    'name' => 'Акции и баннеры',
    'route' => 'admin.promotion.index',
    'sort' => 2,
    'icon-class' => 'temp-icon',
    ], [
        'key'        => 'promotion.promotions',
        'name'       => 'Акции и баннеры',
        'route'      => 'admin.promotion.index',
        'sort'       => 1,
        'icon-class' => '',
    ]
];