<?php

namespace Webkul\Promotion\DataGrids;

use Illuminate\Support\Facades\DB;
use Webkul\Ui\DataGrid\DataGrid;

class PromotionDataGrid extends DataGrid
{
    protected $index = 'id';

    protected $sortOrder = 'desc';

    public function prepareQueryBuilder()
    {
        $queryBuilder = DB::table('promotions')
//            ->leftJoin('categories', 'promotions.category_id', '=', 'categories.id')
//            ->leftJoin('category_translations', 'category_translations.category_id', '=', 'promotions.category_id')
            ->select(
                'promotions.id',
                'promotions.type',
                'promotions.category_id',
                'promotions.start_date',
                'promotions.end_date',
                'promotions.title',
                'promotions.status',
                'promotions.seller_description',
                'promotions.description',
            );

        $this->setQueryBuilder($queryBuilder);
    }

    public function addColumns()
    {
        $this->addColumn([
            'index'      => 'id',
            'label'      => 'Id',
            'type'       => 'number',
            'searchable' => false,
            'sortable'   => true,
            'filterable' => true,
        ]);

        $this->addColumn([
            'index'      => 'type',
            'label'      => 'Тип',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
            'closure'    => function ($row) {
                return $row->type == 'banner' ? 'Баннер' : 'Акция';
            },
        ]);

        $this->addColumn([
            'index'      => 'title',
            'label'      => 'Название',
            'type'       => 'string',
            'searchable' => true,
            'sortable'   => true,
            'filterable' => true,
        ]);

    }

    public function prepareActions()
    {
        $this->addAction([
            'title'  => trans('admin::app.datagrid.edit'),
            'method' => 'GET',
            'route'  => 'admin.promotion.edit',
            'icon'   => 'icon pencil-lg-icon',
        ]);

        $this->addAction([
            'title'  => trans('admin::app.datagrid.delete'),
            'method' => 'POST',
            'route'  => 'admin.promotion.delete',
            'icon'   => 'icon trash-icon',
        ]);
    }
}