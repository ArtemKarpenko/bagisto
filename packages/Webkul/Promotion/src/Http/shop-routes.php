<?php

Route::group([
        'prefix'     => 'promotion',
        'middleware' => ['web', 'theme', 'locale', 'currency']
    ], function () {

        Route::get('/', 'Webkul\Promotion\Http\Controllers\Shop\PromotionController@index')->defaults('_config', [
            'view' => 'promotion::shop.index',
        ])->name('shop.promotion.index');

});