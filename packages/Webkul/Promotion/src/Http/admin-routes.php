<?php

Route::group([
        'prefix'        => 'admin/promotion',
        'middleware'    => ['web', 'admin']
    ], function () {

        Route::get('', 'Webkul\Promotion\Http\Controllers\Admin\PromotionController@index')->defaults('_config', [
            'view' => 'promotion::admin.promotion.index',
        ])->name('admin.promotion.index');

        Route::get('/create-banner', 'Webkul\Promotion\Http\Controllers\Admin\PromotionController@createBanner')->defaults('_config', [
            'view' => 'promotion::admin.promotion.banner.create',
        ])->name('admin.promotion.banner.create');

        Route::get('/create-promotion', 'Webkul\Promotion\Http\Controllers\Admin\PromotionController@createPromotion')->defaults('_config', [
            'view' => 'promotion::admin.promotion.promotion.create',
        ])->name('admin.promotion.promotion.create');

        Route::post('/create', 'Webkul\Promotion\Http\Controllers\Admin\PromotionController@store')->defaults('_config', [
            'redirect' => 'admin.promotion.index',
        ])->name('admin.promotion.store');

        Route::get('/edit/{id}', 'Webkul\Promotion\Http\Controllers\Admin\PromotionController@edit')->defaults('_config', [
            'view_banner' => 'promotion::admin.promotion.banner.edit',
            'view_promotion' => 'promotion::admin.promotion.promotion.edit',
        ])->name('admin.promotion.edit');

        Route::post('/edit/{id}', 'Webkul\Promotion\Http\Controllers\Admin\PromotionController@update')->defaults('_config', [
            'redirect' => 'admin.promotion.index',
        ])->name('admin.promotion.update');

        Route::post('/delete/{id}', 'Webkul\Promotion\Http\Controllers\Admin\PromotionController@destroy')->name('admin.promotion.delete');

});