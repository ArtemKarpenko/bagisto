<?php

namespace Webkul\Promotion\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Webkul\Promotion\Repositories\PromotionRepository;

class PromotionController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * PromotionRepository object
     *
     * @var Webkul\Promotion\Repositories\PromotionRepository
     */
    protected $promotionRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PromotionRepository $promotionRepository)
    {
        $this->middleware('admin');

        $this->_config = request('_config');

        $this->promotionRepository = $promotionRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function createBanner()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function createPromotion()
    {
        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'title'  => 'required',
        ]);

        $data = request()->all();

        $type = $data['type'] == 'banner' ? 'Баннер' : 'Акция';

        $promotion = $this->promotionRepository->create($data);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => $type]));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $promotion = $this->promotionRepository->findOrFail($id);

        $view = $promotion->type == 'banner' ? 'view_banner' : 'view_promotion';

        return view($this->_config[$view], compact('promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'title'  => 'required',
        ]);

        $data = request()->all();

        $data['status'] = !array_key_exists('status', $data) ? false : true;

        $this->promotionRepository->update($data, $id);

        $type = $data['type'] == 'banner' ? 'Баннер' : 'Акция';

        session()->flash('success', trans('admin::app.response.update-success', ['name' => $type]));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }
}
