<?php

namespace Webkul\Promotion\Repositories;

use Webkul\Core\Eloquent\Repository;

class PromotionRepository extends Repository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Webkul\Promotion\Contracts\Promotion';
    }
}