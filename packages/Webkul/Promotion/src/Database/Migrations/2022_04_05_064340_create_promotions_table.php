<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePromotionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promotions', function (Blueprint $table) {
            $table->increments('id')->comment('Идентификатор акции или баннера');
            $table->string('type')->default('banner')->comment('Тип записи (promotion - акция, banner - баннер)');
            $table->integer('category_id')->unsigned()->nullable()->comment('Идентификатор категории для перехода на нее при клике на баннер');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->date('start_date')->nullable()->comment('Дата начала действия акции');
            $table->date('end_date')->nullable()->comment('Дата конца действия акции');
            $table->string('title')->comment('Наименование акции/баннера');
            $table->boolean('status')->default(0)->comment('Статус видимости акции/баннера');
            $table->text('seller_description')->nullable()->comment('Описание для продавца');
            $table->text('description')->nullable()->comment('Описание - у баннера, Описание для покупателя - у акции');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('promotions');
    }
}
