<?php

namespace Webkul\Promotion\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Promotion\Contracts\Promotion as PromotionContract;

class Promotion extends Model implements PromotionContract
{
    protected $fillable = ['type', 'category_id', 'start_date', 'end_date', 'title', 'status', 'seller_description', 'description'];
}