<?php

return [
    'admin' => [
        'system' => [
            'taxes' => [
                'taxes'                        => 'Налоги',
                'catalogue'                    => 'Каталог',
                'pricing'                      => 'Цены',
                'tax-inclusive'                => 'Включая налог',
                'default-location-calculation' => 'Расчет местоположения по умолчанию',
                'default-country'              => 'Страна по умолчанию',
                'default-state'                => 'Штат по умолчанию',
                'default-post-code'            => 'Почтовый индекс по умолчанию',
            ],
        ]
    ]
];
