<?php

namespace Webkul\HomeCategory\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Event::listen([
            'catalog.homecategory.create.after',
            'catalog.homecategory.update.after',
        ], 'Webkul\HomeCategory\Helpers\AdminHelper@storeHomeCategoryIcon');
    }
}
