<?php

namespace Webkul\HomeCategory\Providers;

use Webkul\Core\Providers\CoreModuleServiceProvider;

class ModuleServiceProvider extends CoreModuleServiceProvider
{
    protected $models = [
        \Webkul\HomeCategory\Models\HomeCategory::class,
        \Webkul\HomeCategory\Models\HomeCategoryTranslation::class,
    ];
}