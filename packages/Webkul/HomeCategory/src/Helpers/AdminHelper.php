<?php

namespace Webkul\HomeCategory\Helpers;

use Illuminate\Support\Facades\Storage;
use Webkul\HomeCategory\Repositories\HomeCategoryRepository;

class AdminHelper
{
    /**
     * HomeCategoryRepository object
     *
     * @var \Webkul\HomeCategory\Repositories\HomeCategoryRepository
     */
    protected $homeCategoryRepository;

    /**
     * Create a new helper instance.
     *
     * @param  \Webkul\HomeCategory\Repositories\HomeCategoryRepository  $homeCategoryRepository
     * @return void
     */
    public function __construct(HomeCategoryRepository $homeCategoryRepository)
    {
        $this->homeCategoryRepository =  $homeCategoryRepository;
    }

    /**
     * @param  string  $locale
     * @return string
     */
    public function saveLocaleImg($locale)
    {
        $data = request()->all();
        $type = 'locale_image';

        $locale = $this->uploadImage($locale, $data, $type);

        return $locale;
    }

    /**
     * @param  \Webkul\HomeCategory\Contracts\HomeCategory  $category
     * @return \Webkul\HomeCategory\Contracts\HomeCategory
     */
    public function storeHomeCategoryIcon($category)
    {
        $data = request()->all();

        if (! $category instanceof \Webkul\HomeCategory\Contracts\HomeCategory) {
            $category = $this->homeCategoryRepository->findOrFail($category);
        }

        $category = $this->uploadImage($category, $data, 'category_icon_path');

        return $category;
    }

    /**
     * @param  \Webkul\Core\Contracts\Slider  $slider
     * @return bool
     */
    public function storeSliderDetails($slider)
    {
        $slider->slider_path = request()->get('slider_path');
        $slider->save();

        return true;
    }

    /**
     * @param  \Illuminate\Database\Eloquent\Model  $slider
     * @param  array  $data
     * @param  string  $type
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function uploadImage($model, $data, $type) {
        if (isset($data[$type])) {
            $request = request();

            foreach ($data[$type] as $imageId => $image) {
                $file = $type . '.' . $imageId;
                $dir = 'velocity/' . $type . '/' . $model->id;

                if ($request->hasFile($file)) {
                    if ($model->{$type}) {
                        Storage::delete($model->{$type});
                    }

                    $model->{$type} = $request->file($file)->store($dir);
                    $model->save();
                }
            }
        } else {
            if ($model->{$type}) {
                Storage::delete($model->{$type});
            }

            $model->{$type} = null;
            $model->save();
        }

        return $model;
    }
}