<?php

namespace Webkul\HomeCategory\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\HomeCategory\Contracts\HomeCategoryTranslation as HomeCategoryTranslationContract;

/**
 * Class CategoryTranslation
 *
 * @package Webkul\Category\Models
 *
 * @property-read string $url_path maintained by database triggers
 */
class HomeCategoryTranslation extends Model implements HomeCategoryTranslationContract
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'slug',
        'meta_title',
        'meta_description',
        'meta_keywords',
        'locale_id',
    ];
}