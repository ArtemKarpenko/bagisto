<?php

return [
    [
        'key'   => 'catalog.homecategory',
        'name'  => 'Категории новые',
        'route' => 'admin.catalog.home_categories.index',
        'sort'  => 3,
    ], [
        'key'   => 'catalog.homecategory.create',
        'name'  => 'admin::app.acl.create',
        'route' => 'admin.catalog.home_categories.create',
        'sort'  => 4,
    ], [
        'key'   => 'catalog.homecategory.edit',
        'name'  => 'admin::app.acl.edit',
        'route' => 'admin.catalog.home_categories.edit',
        'sort'  => 5,
    ], [
        'key'   => 'catalog.homecategory.delete',
        'name'  => 'admin::app.acl.delete',
        'route' => 'admin.catalog.home_categories.delete',
        'sort'  => 6,
    ], [
        'key'   => 'catalog.homecategory.mass-delete',
        'name'  => 'admin::app.acl.mass-delete',
        'route' => 'admin.catalog.home_categories.massdelete',
        'sort'  => 7,
    ]
];