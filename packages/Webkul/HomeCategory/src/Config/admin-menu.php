<?php

return [
    [
        'key'        => 'catalog.homecategory',
        'name'       => 'Категории новые',
        'route'      => 'admin.catalog.home_categories.index',
        'sort'       => 3,
        'icon-class' => '',
    ]
];