<?php

Route::group(['middleware' => ['web', 'admin_locale']], function () {
    Route::prefix(config('app.admin_url'))->group(function () {
        // Admin Routes
        Route::group(['middleware' => ['admin']], function () {
            // Catalog Routes
            Route::prefix('catalog')->group(function () {
                // Catalog Home Category Routes
                Route::get('/homecategory', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@index')->defaults('_config', [
                    'view' => 'homecategory::admin.index',
                    'test' => 'test'
                ])->name('admin.catalog.home_categories.index');

                Route::get('/homecategory/create', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@create')->defaults('_config', [
                    'view' => 'homecategory::admin.create',
                ])->name('admin.catalog.home_categories.create');

                Route::post('/homecategory/create', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@store')->defaults('_config', [
                    'redirect' => 'admin.catalog.home_categories.index',
                ])->name('admin.catalog.home_categories.store');

                Route::get('/homecategory/edit/{id}', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@edit')->defaults('_config', [
                    'view' => 'homecategory::admin.edit',
                ])->name('admin.catalog.home_categories.edit');

                Route::put('/homecategory/edit/{id}', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@update')->defaults('_config', [
                    'redirect' => 'admin.catalog.home_categories.index',
                ])->name('admin.catalog.home_categories.update');

                Route::post('/homecategory/delete/{id}', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@destroy')->name('admin.catalog.home_categories.delete');


                //home category massdelete
                Route::post('homecategory/massdelete', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@massDestroy')->defaults('_config', [
                    'redirect' => 'admin.catalog.home_categories.index',
                ])->name('admin.catalog.home_categories.massdelete');

                Route::post('/homecategory/product/count', 'Webkul\HomeCategory\Http\Controllers\Admin\HomeCategoryController@categoryProductCount')->name('admin.catalog.home_categories.product.count');
            });
        });
    });
});
