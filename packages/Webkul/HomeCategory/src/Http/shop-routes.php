<?php

Route::group([
        'prefix'     => 'homecategory',
        'middleware' => ['web', 'theme', 'locale', 'currency']
    ], function () {

        Route::get('/', 'Webkul\HomeCategory\Http\Controllers\Shop\HomeCategoryController@index')->defaults('_config', [
            'view' => 'homecategory::shop.index',
        ])->name('shop.homecategory.index');

});