<?php

Route::group([
        'prefix'     => 'color',
        'middleware' => ['web', 'theme', 'locale', 'currency']
    ], function () {

        Route::get('/', 'Webkul\Color\Http\Controllers\Shop\ColorController@index')->defaults('_config', [
            'view' => 'color::shop.index',
        ])->name('shop.color.index');

});