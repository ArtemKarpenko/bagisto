<?php

Route::group([
        'prefix'        => 'admin/color',
        'middleware'    => ['web', 'admin']
    ], function () {

        Route::get('', 'Webkul\Color\Http\Controllers\Admin\ColorController@index')->defaults('_config', [
            'view' => 'color::admin.color.index',
        ])->name('admin.color.index');

        Route::get('/create', 'Webkul\Color\Http\Controllers\Admin\ColorController@create')->defaults('_config', [
            'view' => 'color::admin.color.create',
        ])->name('admin.color.create');

        Route::post('/create', 'Webkul\Color\Http\Controllers\Admin\ColorController@store')->defaults('_config', [
            'redirect' => 'admin.color.index',
        ])->name('admin.color.store');

        Route::get('/edit/{id}', 'Webkul\Color\Http\Controllers\Admin\ColorController@edit')->defaults('_config', [
            'view' => 'color::admin.color.edit',
        ])->name('admin.color.edit');

        Route::post('/edit/{id}', 'Webkul\Color\Http\Controllers\Admin\ColorController@update')->defaults('_config', [
            'redirect' => 'admin.color.index',
        ])->name('admin.color.update');

        Route::post('/delete/{id}', 'Webkul\Color\Http\Controllers\Admin\ColorController@destroy')->name('admin.color.delete');

});