<?php

namespace Webkul\Color\Http\Controllers\Admin;

use Illuminate\Routing\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Webkul\Color\Repositories\ColorRepository;

class ColorController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Contains route related configuration
     *
     * @var array
     */
    protected $_config;

    /**
     * ColorRepository object
     *
     * @var Webkul\Color\Repositories\ColorRepository
     */
    protected $colorRepository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ColorRepository $colorRepository)
    {
        $this->middleware('admin');

        $this->colorRepository = $colorRepository;

        $this->_config = request('_config');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view($this->_config['view']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        return view($this->_config['view']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        $this->validate(request(), [
            'title'  => 'required',
            'hex_code' => 'required',
        ]);

        $data = request()->all();

        $color = $this->colorRepository->create($data);

        session()->flash('success', trans('admin::app.response.create-success', ['name' => 'Цвет']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $color = $this->colorRepository->findOrFail($id);

        return view($this->_config['view'], compact('color'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $this->validate(request(), [
            'title'  => 'required',
            'hex_code' => 'required',
        ]);

        $data = request()->all();

        $this->colorRepository->update($data, $id);

        session()->flash('success', trans('admin::app.response.update-success', ['name' => 'Цвет']));

        return redirect()->route($this->_config['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->colorRepository->delete($id);

            session()->flash('success', trans('admin::app.response.delete-success', ['name' => 'Цвет']));

            return response()->json(['message' => true], 200);
        } catch(\Exception $e) {
            session()->flash('error', trans('admin::app.response.delete-failed', ['name' => 'Цвет']));
        }

        return response()->json(['message' => false], 400);
    }
}
