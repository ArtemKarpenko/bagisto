<?php

namespace Webkul\Color\Models;

use Illuminate\Database\Eloquent\Model;
use Webkul\Color\Contracts\Color as ColorContract;

class Color extends Model implements ColorContract
{
    public $timestamps = false;

    protected $fillable = ['title', 'hex_code', '1c_id'];
}