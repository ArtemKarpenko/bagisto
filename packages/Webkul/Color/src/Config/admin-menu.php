<?php

return [
    [
        'key' => 'color',
        'name' => 'Цвета',
        'route' => 'admin.color.index',
        'sort' => 2,
        'icon-class' => 'temp-icon',
    ], [
        'key'        => 'color.colors',
        'name'       => 'Цвета',
        'route'      => 'admin.color.index',
        'sort'       => 1,
        'icon-class' => '',
    ]
];