@extends('admin::layouts.content')

@section('page_title')
    Все цвета
@stop

@section('content')

    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h1>Цвета</h1>
            </div>
            <div class="page-action">
                <a href="{{ route('admin.color.create') }}" class="btn btn-lg btn-primary">
                    Добавить цвет
                </a>
            </div>
        </div>

        <div class="page-content">
            @inject('colorGroup','Webkul\Color\DataGrids\ColorDataGrid')
            {!! $colorGroup->render() !!}
        </div>
    </div>

@stop