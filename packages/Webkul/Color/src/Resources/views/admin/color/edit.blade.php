@extends('admin::layouts.content')

@section('page_title')
    Добавить Цвет
@stop

@section('content')
    <div class="content">
        <form method="POST" action="{{ route('admin.color.update', $color->id) }}" @submit.prevent="onSubmit">

            <div class="page-header">
                <div class="page-title">
                    <h1>
                        <i class="icon angle-left-icon back-link" onclick="window.location = '{{ route('admin.color.index') }}'"></i>

                        Добавить Цвет
                    </h1>
                </div>

                <div class="page-action">
                    <button type="submit" class="btn btn-lg btn-primary">
                        Сохранить Цвет
                    </button>
                </div>
            </div>

            <div class="page-content">
                <div class="form-container">
                    @csrf()

                    <div class="control-group" :class="[errors.has('title') ? 'has-error' : '']">
                        <label for="title" class="required">
                            Наименование
                        </label>
                        <input type="text" class="control" name="title" v-validate="'required'" value="{{ old('name') ?: $color->title }}" data-vv-as="&quot;Наименование&quot;">
                        <span class="control-error" v-if="errors.has('title')">@{{ errors.first('title') }}</span>
                    </div>

                    <div class="control-group" :class="[errors.has('hex_code') ? 'has-error' : '']">
                        <label for="title" class="required mb-10">
                            Цвет
                        </label>
                        <swatch-picker :input-name="'hex_code'" :color="'{{ $color->hex_code }}'" colors="text-advanced" show-fallback />
                        <span class="control-error" v-if="errors.has('hex_code')">@{{ errors.first('hex_code') }}</span>
                    </div>

                </div>
            </div>

        </form>
    </div>
@stop